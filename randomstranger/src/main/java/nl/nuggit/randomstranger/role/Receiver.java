package nl.nuggit.randomstranger.role;

public interface Receiver {

    public void receive(String name, String input);
}
