package nl.nuggit.randomstranger.mood;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import nl.nuggit.randomstranger.role.Actor;

public class Temper extends Actor {

    private Map<Mood, Integer> moodValues = new HashMap<Mood, Integer>();

    public Temper() {
        Calendar now = Calendar.getInstance();
        for (Mood mood : Mood.values()) {
            int delta = mood.getDelta(now);
            System.err.println(String.format("Setting mood %s to %s", mood, delta));
            moodValues.put(mood, delta);
        }
        start();
    }

    @Override
    public void run() {
        while (!isStopped) {
            pause(TimeUnit.MINUTES, 22);
            Calendar now = Calendar.getInstance();
            for (Mood mood : Mood.values()) {
                change(mood, mood.getDelta(now));
            }
        }
    }

    public synchronized int get(Mood mood) {
        return moodValues.get(mood);
    }

    public synchronized void set(Mood mood, int value) {
        moodValues.put(mood, value);
    }

    public synchronized void change(Mood mood, int delta) {
        int newValue = (int) moodValues.get(mood) + delta;
        System.err.println(String.format("Setting mood %s to %s", mood, newValue));
        moodValues.put(mood, newValue);
    }
}
