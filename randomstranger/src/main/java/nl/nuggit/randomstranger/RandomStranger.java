package nl.nuggit.randomstranger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RandomStranger {
    public static void main(String[] args) throws IOException {
        
        ChatBot chatBot = new ChatBot(System.out);
        
        String userName = "user";
        BufferedReader user = new BufferedReader(new InputStreamReader(System.in));
        String userInput;
        while ((userInput = user.readLine()) != null) {
            System.out.println(String.format("%s : %s", userName, userInput));
            chatBot.receive(userName, userInput);
        }
    }
}
