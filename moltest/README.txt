index -> login
  registration_open=true -> register new user
  registration_open=false -> login existing user
  set player in session
-> home

home
  no player -> index
  player not active -> elimination
  no test -> wait
  registration_open -> wait
  player has not completed test, set new result in session -> test
  other players have not completed test true -> wait, false -> elimination

test
  no player -> index
  no test -> index
  no ongoing result -> index



