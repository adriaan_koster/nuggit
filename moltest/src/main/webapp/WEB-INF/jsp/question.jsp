<%@ page import="nl.nuggit.moltest.model.Player" %>
<%@ page import="nl.nuggit.moltest.model.Test" %>
<%@ page import="nl.nuggit.moltest.model.question.Question" %>
<%@ page import="nl.nuggit.moltest.DataService" %>
<%@ page import="nl.nuggit.moltest.model.Result" %>
<HTML>
<HEAD>
    <TITLE>Wie kent de Mol?</TITLE>
</HEAD>
<BODY style="font-size: larger">
<%
    Player player = (Player) session.getAttribute("player");
%>

<h1>Test maken</h1>

Hallo, <%= player.getName()%>.

<%
    Result result = (Result) session.getAttribute("result");
%>

<h1><%=result.getTest().getTitle()%>
</h1>

<%= result.nextQuestion().getText() %>
<BR/>
<BR/>
<%= result.nextQuestion().getOptions() %>

<%
    if (request.getParameter("message") != null) {
        out.println("<p style=\"color:red;\">" + request.getParameter("message") + "</p>");
    }
%>

<FORM METHOD="POST" ACTION="test">
    <INPUT TYPE="NUMBER" NAME="answerText" SIZE="30"/> <BR/>
    <BR/>
    <INPUT TYPE=SUBMIT>
    <BR/>
</FORM>
</BODY>
</HTML>