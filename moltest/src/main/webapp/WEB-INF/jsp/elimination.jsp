<%@ page import="nl.nuggit.moltest.model.Player" %>
<%@ page import="nl.nuggit.moltest.DataService" %>
<%@ page import="nl.nuggit.moltest.model.Result" %>
<HTML>
<HEAD>
    <TITLE>Wie kent de Mol?</TITLE>
</HEAD>
<BODY style="font-size: larger">
<%
    Player player = (Player) session.getAttribute("player");
    Result lastResult = DataService.findLastResult(player);
%>

<h1>Eliminatie</h1>

Hallo, <%= player.getName() %>.
<%
    if(lastResult != null) {
%>
Je hebt de test <%= lastResult.getTest().getTitle() %> gemaakt.
<%
    }
%>

Hier komt de uitslag...

<%
    String color;
    if (player.isActive()) {
        color = "green";
    } else {
        color = "red";
    }
%>

<SCRIPT LANGUAGE="JavaScript">
    setTimeout("document.body.style.background = \"<%= color %>\"", 5000);
</SCRIPT>

</BODY>
</HTML>