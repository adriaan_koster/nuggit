<%@ page import="nl.nuggit.moltest.model.Player" %>
<%@ page import="nl.nuggit.moltest.DataService" %>
<HTML>
<HEAD>
    <TITLE>Wie kent de Mol?</TITLE>
</HEAD>
<BODY style="font-size: larger">
<%
    Player player = (Player) session.getAttribute("player");
%>

<h1>Nog even geduld...</h1>

Hallo, <%= player.getName()%>.

<%
    if(DataService.registrationOpen || DataService.currentTestId == 0) {
%>
We wachten tot alle spelers zijn aangemeld.
<%
    } else {
%>
Nog <%= DataService.countPlayersToMakeTest()%> andere spelers moeten de test maken voordat de uitslag bekend is.
<%
    }
%>

</BODY>
</HTML>