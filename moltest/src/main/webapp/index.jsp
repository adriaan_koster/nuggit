<%@ page import="nl.nuggit.moltest.DataService" %>
<%@ page import="nl.nuggit.moltest.model.Player" %>
<HTML>
<HEAD>
    <TITLE>Wie kent de Mol?</TITLE>
</HEAD>
<BODY style="font-size: larger">

<%
    Player player = (Player) session.getAttribute("player");
    if (player != null) {
        request.getRequestDispatcher("home").forward(request, response);
    }
%>

<%
    if (request.getParameter("message") != null) {
        out.println("<p style=\"color:red;\">" + request.getParameter("message") + "</p>");
    }
%>

<FORM METHOD="POST" ACTION="login">
<%
    if (DataService.registrationOpen) {
%>
        <h1>Registreer</h1>
        Naam:<BR/>
        <INPUT TYPE="TEXT" NAME="name" SIZE="30"/><BR/>
        Geboortedatum:<BR/>
        <INPUT TYPE="TEXT" PLACEHOLDER="dd-mm-jjjj" NAME="dateOfBirth" SIZE="10"/><BR/>
<%
    }
    else {
%>
        <h1>Login</h1>
<%
    }
%>

    Wachtwoord:<BR/>
    <INPUT TYPE="PASSWORD" NAME="password" SIZE="20"/><BR/>
    <BR/>
    <INPUT TYPE=SUBMIT>
    <BR/>

</FORM>
</BODY>
</HTML>