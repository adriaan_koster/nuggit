package nl.nuggit.moltest;

import nl.nuggit.moltest.model.Answer;
import nl.nuggit.moltest.model.Player;
import nl.nuggit.moltest.model.PlayerStatus;
import nl.nuggit.moltest.model.Result;
import nl.nuggit.moltest.model.ResultsLowestFirstComparator;
import nl.nuggit.moltest.model.Test;
import nl.nuggit.moltest.model.question.Question;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class DataService implements ServletContextListener {

	public static final String REGISTRATION_OPEN = "registration_open";
	public static final String WIE_IS_DE_MOL = "wie_is_de_mol";
	public static final String PLAYER = "player";
	public static final String CURRENT_TEST = "current_test";
	public static final String NUMBER_OF_PLAYERS_TO_ELIMININATE = "number_of_players_to_eliminate";
	public static final String TEST = "test";
	public static final String QUESTION = "question";
	public static final String RESULT = "result";
	public static final String ANSWER = "answer";

	public static List<Player> players = new ArrayList<>();
	public static List<Test> tests = new ArrayList<>();
	public static List<Result> results = new ArrayList<>();
	public static boolean registrationOpen;
	public static int wieIsDeMol;
	public static int currentTestId;
	public static int numberOfPlayersToEliminate;

	private static File playersFile;
	private static File testsFile;
	private static File resultsFile;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("ServletContextListener destroyed");
	}

	@Override
	public void contextInitialized(ServletContextEvent e) {
		System.out.println("ServletContextListener started");
		playersFile = new File(e.getServletContext().getRealPath("/WEB-INF/classes/players.txt"));
		testsFile = new File(e.getServletContext().getRealPath("/WEB-INF/classes/tests.txt"));
		resultsFile = new File(e.getServletContext().getRealPath("/WEB-INF/classes/results.txt"));
		init();
	}

	public static void init() {
		initPlayers();
		initTests();
		initResults();
	}

	private static void initPlayers() {
		int active = 0;
		try (Scanner scanner = new Scanner(playersFile)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().trim();
				if (line.startsWith(PLAYER)) {
					Player player = new Player(line.trim());
					players.add(player);
					if (player.isActive()) {
						active++;
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.printf("Read %s players, %s active\n", players.size(), active);
	}

	private static void initTests() {
		int t = 0;
		int q = 0;
		try (Scanner scanner = new Scanner(testsFile)) {
			Test test = null;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().trim();
				if (line.startsWith(REGISTRATION_OPEN)) {
					parseRegistrationOpen(line);
				} else if (line.startsWith(CURRENT_TEST)) {
					parseCurrentTest(line);
				} else if (line.startsWith(NUMBER_OF_PLAYERS_TO_ELIMININATE)) {
					parseNumberOfPlayersToEliminate(line);
				} else if (line.startsWith(WIE_IS_DE_MOL)) {
					parseWieIsDeMol(line);
				} else if (line.startsWith(TEST)) {
					test = new Test(line);
					tests.add(test);
					t++;
				} else if (line.startsWith(QUESTION)) {
					if (test == null) {
						throw new IllegalArgumentException(String.format("Found %s without %s: %s", QUESTION, TEST, line));
					}
					Question question = Question.getInstance(line);
					test.getQuestions().add(question);
					q++;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.printf("Read %s tests, %s questions, %s = %s, %s = %s, %s = %s\n", t, q, REGISTRATION_OPEN, registrationOpen, CURRENT_TEST, currentTestId, NUMBER_OF_PLAYERS_TO_ELIMININATE, numberOfPlayersToEliminate);
	}

	private static void parseRegistrationOpen(String line) {
		String[] fields = line.split(",");
		int i = 0;
		if (fields.length != 2 || !fields[i++].equals(REGISTRATION_OPEN)) {
			throw new IllegalArgumentException(String.format("Invalid %s: %s", REGISTRATION_OPEN, line));
		}
		registrationOpen = Boolean.parseBoolean(fields[i++]);
	}

	private static void parseCurrentTest(String line) {
		String[] fields = line.split(",");
		int i = 0;
		if (fields.length != 2 || !fields[i++].equals(CURRENT_TEST)) {
			throw new IllegalArgumentException(String.format("Invalid %s: %s", CURRENT_TEST, line));
		}
		currentTestId = Integer.parseInt(fields[i++]);
	}

	private static void parseWieIsDeMol(String line) {
		String[] fields = line.split(",");
		int i = 0;
		if (fields.length != 2 || !fields[i++].equals(WIE_IS_DE_MOL)) {
			throw new IllegalArgumentException(String.format("Invalid %s: %s", WIE_IS_DE_MOL, line));
		}
		wieIsDeMol = Integer.parseInt(fields[i++]);
	}

	private static void parseNumberOfPlayersToEliminate(String line) {
		String[] fields = line.split(",");
		int i = 0;
		if (fields.length != 2 || !fields[i++].equals(NUMBER_OF_PLAYERS_TO_ELIMININATE)) {
			throw new IllegalArgumentException(String.format("Invalid %s: %s", NUMBER_OF_PLAYERS_TO_ELIMININATE, line));
		}
		numberOfPlayersToEliminate = Integer.parseInt(fields[i++]);
	}

	private static void initResults() {
		int r = 0;
		int a = 0;
		try (Scanner scanner = new Scanner(resultsFile)) {
			Result result = null;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().trim();
				if (line.startsWith(RESULT)) {
					result = new Result(line);
					results.add(result);
					r++;
				} else if (line.startsWith(ANSWER)) {
					if (result == null) {
						throw new IllegalArgumentException(String.format("Found %s without %s: %s", ANSWER, RESULT, line));
					}
					Answer answer = new Answer(line);
					result.getAnswers().add(answer);
					a++;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.printf("Read %s results, %s answers\n", r, a);
	}

	public static Player findPlayer(String password) {
		for (Player player : players) {
			if (player.getPassword().equals(password)) {
				return player;
			}
		}
		return null;
	}

	public static Player findPlayer(int id) {
		for (Player player : players) {
			if (player.getId() == id) {
				return player;
			}
		}
		return null;
	}

	public static int generateNextPlayerId() {
		int maxId = 0;
		for (Player player : players) {
			if (player.getId() > maxId) {
				maxId = player.getId();
			}
		}
		return maxId + 1;
	}

	public static int countActivePlayers() {
		int count = 0;
		for (Player player : players) {
			if (player.isActive()) {
				count++;
			}
		}
		return count;
	}

	public static List<Player> getActivePlayers() {
		List<Player> activePlayers = new ArrayList<>();
		for (Player player : players) {
			if (player.isActive()) {
				activePlayers.add(player);
			}
		}
		return activePlayers;
	}

	public static void storePlayer(Player player) {
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(playersFile, true)))) {
			out.println(player.toString());
			players.add(player);
		} catch (IOException e) {
			throw new IllegalArgumentException("Could not store player", e);
		}
	}

	public static Test findTest(int id) {
		for (Test test : tests) {
			if (test.getId() == id) {
				return test;
			}
		}
		return null;
	}

	public static Result findCurrentResult(Player player) {
		Result currentResult = null;
		for (Result result : results) {
			if (player.getId() == result.getPlayer().getId() && result.getTest().getId() == currentTestId) {
				currentResult = result;
			}
		}
		return currentResult;
	}

	public static Result findLastResult(Player player) {
		Result lastResult = null;
		for (Result result : results) {
			if (player.getId() == result.getPlayer().getId() &&
					(lastResult == null || lastResult.getTest().getId() < result.getTest().getId())) {
				lastResult = result;
			}
		}
		return lastResult;
	}

	public static void storeResult(Result result) {
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(resultsFile, true)))) {
			out.println(result.toString());
			for (Answer answer : result.getAnswers()) {
				out.println(answer.toString());
			}
			results.add(result);
		} catch (IOException e) {
			throw new IllegalArgumentException("Could not store result", e);
		}
	}

	public static int countPlayersToMakeTest() {
		int completedTests = 0;
		for (Result result : results) {
			if (result.getTest().getId() == currentTestId) {
				completedTests++;
			}
		}
		return Math.max(0, countActivePlayers() - completedTests);
	}

	public static synchronized void eliminatePlayers() {
		if (countPlayersToMakeTest() > 0) {
			throw new IllegalStateException("Cannot eliminate players before test is completed");
		}
		List<Result> sortedResults = new ArrayList<>(results);
		Collections.sort(sortedResults, new ResultsLowestFirstComparator());
		int eliminatedPlayers = 0;
		int i = 0;
		while (eliminatedPlayers < numberOfPlayersToEliminate) {
			Result eliminated = sortedResults.get(i++);
			Player player = eliminated.getPlayer();
			if (player.getId() != wieIsDeMol) {
				player.setStatus(PlayerStatus.ELIMINATED);
				updatePlayer(player);
				eliminatedPlayers++;
			}
		}
	}

	private static synchronized boolean updatePlayer(Player player) {
		File tempFile = new File(String.format("moltest_temp_players_%s.txt", System.currentTimeMillis()));
		try (BufferedReader reader = new BufferedReader(new FileReader(playersFile));
		     BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile))) {
			String line;
			while ((line = reader.readLine()) != null) {
				String updatedLine = null;
				if (line.startsWith(PLAYER)) {
					Player oldPlayer = new Player(line.trim());
					if (oldPlayer.getId() == player.getId()) {
						updatedLine = player.toString();
					}
				}

				if (updatedLine == null) {
					updatedLine = line;
				}
				writer.write(updatedLine);
				writer.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		playersFile.delete();
		boolean success = tempFile.renameTo(playersFile);
		if (!success) {
			System.out.println("Could not rename tempfile: " + tempFile);
		}
		return success;
	}
}
