package nl.nuggit.moltest;

import nl.nuggit.moltest.model.Player;
import nl.nuggit.moltest.model.Result;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HomeServlet extends BaseServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Player player = (Player) request.getSession().getAttribute("player");
		if (player == null) {
			viewIndex(request, response, "Je bent niet ingelogd");
			return;
		}
		if (!player.isActive()) {
			viewElimination(request, response);
			return;
		}
		if (DataService.currentTestId == 0) {
			viewWait(request, response);
			return;
		}
		if (DataService.registrationOpen) {
			viewWait(request, response);
			return;
		}
		Result result = DataService.findCurrentResult(player);
		if (result == null) {
			viewTest(request, response);
			return;
		}
		if (DataService.countPlayersToMakeTest() > 0) {
			viewWait(request, response);
			return;
		}
		viewElimination(request, response);
	}
}
