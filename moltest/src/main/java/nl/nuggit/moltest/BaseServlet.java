package nl.nuggit.moltest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class BaseServlet extends HttpServlet {

	void viewIndex(HttpServletRequest request, HttpServletResponse response, String message) throws ServletException, IOException {
		request.getRequestDispatcher("/index.jsp" + (message == null ? "" : "?message=" + message)).forward(request, response);
	}

	void viewHome(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("home").forward(request, response);
	}

	void viewTest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("test").forward(request, response);
	}

	void viewQuestion(HttpServletRequest request, HttpServletResponse response, String message) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/question.jsp" + (message == null ? "" : "?message=" + message)).forward(request, response);
	}

	void viewWait(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/wait.jsp").forward(request, response);
	}

	void viewElimination(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/elimination.jsp").forward(request, response);
	}
}
