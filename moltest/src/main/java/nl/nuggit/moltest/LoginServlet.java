package nl.nuggit.moltest;

import nl.nuggit.moltest.model.Player;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends BaseServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Player player;
		if (DataService.registrationOpen) {
			player = registerPlayer(request, response);
		} else {
			player = loginPlayer(request, response);
		}
		if (player != null) {
			request.getSession().invalidate();
			request.getSession().setAttribute("player", player);
			viewHome(request, response);
		}
	}

	private Player registerPlayer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Player player = new Player(request.getParameter("name"),
				request.getParameter("dateOfBirth"),
				request.getParameter("password"));
		String validationError = player.validationError();
		if (validationError != null) {
			request.getSession().invalidate();
			viewIndex(request, response, validationError);
			player = null;
		} else {
			DataService.storePlayer(player);
		}
		return player;
	}

	private Player loginPlayer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Player player = DataService.findPlayer(request.getParameter("password"));
		if (player == null) {
			viewIndex(request, response, "Wachtwoord onbekend");
		}
		return player;
	}
}
