package nl.nuggit.moltest.model;

public enum PlayerStatus {
    ACTIVE,
    ELIMINATED
}
