package nl.nuggit.moltest.model;

import nl.nuggit.moltest.DataService;

public class Player {

	private final int id;
	private final String name;
	private final String password;
	private final String dateOfBirth;
	private PlayerStatus status;

	public Player(String name, String dateOfBirth, String password) {
		this.id = DataService.generateNextPlayerId();
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.password = password;
		this.status = PlayerStatus.ACTIVE;
	}

	public Player(String spec) {
		String[] fields = spec.split(",");
		int i = 0;
		if (fields.length != 6 || !fields[i++].equals("player")) {
			throw new IllegalArgumentException("Invalid player: " + spec);
		}
		this.id = Integer.parseInt(fields[i++]);
		this.name = fields[i++];
		this.password = fields[i++];
		this.dateOfBirth = fields[i++];
		this.status = PlayerStatus.valueOf(fields[i++]);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public void setStatus(PlayerStatus status) {
		this.status = status;
	}

	public String validationError() {
		String error;
		validation:
		{
			if (id < 1) {
				error = "Illegal id: " + id;
				break validation;
			}
			if (name == null || name.length() < 1) {
				error = "Name too short";
				break validation;
			}
			if (password == null || password.length() < 6) {
				error = "Password too short";
				break validation;
			}
			Player player = DataService.findPlayer(password);
			if (player != null && player.getId() != id) {
				error = "Duplicate user";
				break validation;
			}
			if (dateOfBirth == null || dateOfBirth.length() != 10) {
				error = "Illegal date of birth: " + dateOfBirth;
				break validation;
			}
			if (status == null) {
				error = "Player status may not be null";
				break validation;
			}
			error = null;
		}
		return error;
	}

	public String toString() {
		return String.format("player,%s,%s,%s,%s,%s", id, name, password, dateOfBirth, status);
	}

	public boolean isActive() {
		return status == PlayerStatus.ACTIVE;
	}
}
