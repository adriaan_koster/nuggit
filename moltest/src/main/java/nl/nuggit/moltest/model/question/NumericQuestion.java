package nl.nuggit.moltest.model.question;

public class NumericQuestion extends Question {

    @Override
    public String getOptions() {
        return "Voer een getal in<BR/>";
    }

    @Override
    public boolean isValid(String answer) {
        try {
            Integer.parseInt(answer);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public int getScore(String answer) {
        if (typeFields.size() < 2) {
            throw new IllegalStateException("Invalid NumericQuestion: " + getText());
        }
        int maxScore = Integer.parseInt(typeFields.get(0));
        int rightAnswer = Integer.parseInt(typeFields.get(1));
        int givenAnswer = Integer.parseInt(answer);
	    return Math.max(maxScore - Math.abs(rightAnswer - givenAnswer), 0);
    }
}
