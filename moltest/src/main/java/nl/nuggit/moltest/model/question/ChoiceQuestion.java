package nl.nuggit.moltest.model.question;

import java.util.ArrayList;
import java.util.List;

public class ChoiceQuestion extends Question {

    private List<Option> options;

    private class Option {
        String text;
        int score;

        Option(String spec) {
            String[] fields = spec.split("=");
            if (fields.length != 2) {
                throw new IllegalArgumentException("Invalid option: " + spec);
            }
            this.text = fields[0];
            this.score = Integer.parseInt(fields[1]);
        }
    }

    @Override
    public String getOptions() {
        if (options == null) {
            options = new ArrayList<>();
            for (String typeField : typeFields) {
                options.add(new Option(typeField));
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < options.size(); i++) {
            Option option = options.get(i);
            sb.append(i + 1).append(" : ").append(option.text).append("<BR/>");
        }
        return sb.toString();
    }

    @Override
    public boolean isValid(String answer) {
        try {
            int answerIndex = Integer.parseInt(answer);
            return options.size() > (answerIndex - 1) && answerIndex >= 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public int getScore(String answer) {
        int answerIndex = Integer.parseInt(answer);
        Option option = options.get(answerIndex - 1);
        return option.score;
    }
}

