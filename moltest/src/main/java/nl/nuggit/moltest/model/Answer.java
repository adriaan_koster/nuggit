package nl.nuggit.moltest.model;

public class Answer {

	private String text;
	private int score;

	public Answer(String text, int score) {
		this.text = text;
		this.score = score;
	}

	public Answer(String spec) {
		String[] fields = spec.split(",");
		int i = 0;
		if (fields.length != 3 || !fields[i++].equals("answer")) {
			throw new IllegalArgumentException("Invalid answer: " + spec);
		}
		this.text = fields[i++];
		this.score = Integer.parseInt(fields[i++]);
	}

	public int getScore() {
		return score;
	}

	public String toString() {
		return String.format("answer,%s,%s", text, score);
	}
}
