package nl.nuggit.moltest.model;

import java.util.Comparator;

public class ResultsLowestFirstComparator implements Comparator<Result> {

    public int compare(Result r1, Result r2) {
        int value = r1.getScore() - r2.getScore();
        if (value == 0) {
            value = r1.getDuration() - r2.getDuration();
        }
        return value;
    }
}
