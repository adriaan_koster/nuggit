package nl.nuggit.moltest.model;

import nl.nuggit.moltest.model.question.Question;

import java.util.ArrayList;
import java.util.List;

public class Test {

	private int id;
	private String title;
	private List<Question> questions = new ArrayList<>();

	public Test(String spec) {
		String[] fields = spec.split(",");
		int i = 0;
		if (fields.length != 3 || !fields[i++].equals("test")) {
			throw new IllegalArgumentException("Invalid test: " + spec);
		}
		id = Integer.parseInt(fields[i++]);
		title = fields[i++];
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public String toString() {
		return String.format("test,%s,%s", id, title);
	}
}
