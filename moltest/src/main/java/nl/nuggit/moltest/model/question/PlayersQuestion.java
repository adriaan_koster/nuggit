package nl.nuggit.moltest.model.question;

import nl.nuggit.moltest.DataService;
import nl.nuggit.moltest.model.Player;

import java.util.List;

public class PlayersQuestion extends Question {

	@Override
	public String getOptions() {
		StringBuilder sb = new StringBuilder();
		List<Player> activePlayers = DataService.getActivePlayers();
		for (int i = 0; i < activePlayers.size(); i++) {
			Player player = activePlayers.get(i);
			int answerNumber = i + 1;
			sb.append(answerNumber).append(" : ").append(player.getName()).append("<BR/>");
		}
		return sb.toString();
	}

	@Override
	public boolean isValid(String answer) {
		try {
			int answerIndex = Integer.parseInt(answer);
			return DataService.getActivePlayers().size() > (answerIndex - 1) && answerIndex >= 0;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	@Override
	public int getScore(String answer) {
		int answerNumber = Integer.parseInt(answer);
		int score = Integer.parseInt(typeFields.get(0));
		int rightAnswer = Integer.parseInt(typeFields.get(1));
		Player player = DataService.getActivePlayers().get(answerNumber - 1);
		return player.getId() == rightAnswer ? score : 0;
	}
}

