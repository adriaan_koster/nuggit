package nl.nuggit.moltest.model.question;

import java.util.ArrayList;
import java.util.List;

public abstract class Question {

	private String text;
	List<String> typeFields;

	public static Question getInstance(String spec) {
		String[] fields = spec.split(",");
		int i = 0;
		if (fields.length < 3  || !fields[i++].equals("question")) {
			throw new IllegalArgumentException("Invalid question: " + spec);
		}
		String text = fields[i++];
		QuestionType type = QuestionType.valueOf(fields[i++]);
		Question impl = type.getImplementation();
		impl.text = text;
		impl.typeFields = new ArrayList<>();
		while (i < fields.length) {
			impl.typeFields.add(fields[i]);
			i++;
		}
		return impl;
	}

	public String getText() {
		return text;
	}

	public abstract String getOptions();

	public abstract boolean isValid(String answer);

	public abstract int getScore(String answer);

	public String toString() {
		StringBuilder sb = new StringBuilder("question,");
		sb.append(text);
		for (String typeField : typeFields) {
			sb.append(",").append(typeField);
		}
		return sb.toString();
	}
}
