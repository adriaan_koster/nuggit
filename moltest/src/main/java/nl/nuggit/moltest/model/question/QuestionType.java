package nl.nuggit.moltest.model.question;

public enum QuestionType {
    NUMERIC(NumericQuestion.class),
    CHOICE(ChoiceQuestion.class),
    PLAYERS(PlayersQuestion.class);

    private Class<? extends Question> implementation;

    private QuestionType(Class<? extends Question> impl) {
        this.implementation = impl;
    }

    public Question getImplementation() {
        try {
            return implementation.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException("Could not create implementation of " + implementation);
    }
}
