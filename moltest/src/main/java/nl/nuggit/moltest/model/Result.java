package nl.nuggit.moltest.model;

import nl.nuggit.moltest.DataService;
import nl.nuggit.moltest.model.question.Question;

import java.util.ArrayList;
import java.util.List;

public class Result {

	private Player player;
	private Test test;
	private long startTime = System.currentTimeMillis();
	private int duration;
	private List<Answer> answers = new ArrayList<>();

	public Result(Player player, Test test) {
		this.player = player;
		this.test = test;
	}

	public Result(String spec) {
		String[] fields = spec.split(",");
		int i = 0;
		if (fields.length != 4 || !fields[i++].equals("result")) {
			throw new IllegalArgumentException("Invalid result: " + spec);
		}
		this.player = DataService.findPlayer(Integer.parseInt(fields[i++]));
		this.test = DataService.findTest(Integer.parseInt(fields[i++]));
		this.duration = Integer.parseInt(fields[i++]);
	}

	public Player getPlayer() {
		return player;
	}

	public Test getTest() {
		return test;
	}

	public int getDuration() {
		return duration;
	}


	public void stopTimer() {
		this.duration = (int) (System.currentTimeMillis() - startTime);
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public int getScore() {
		int score = 0;
		for (Answer answer : answers) {
			score += answer.getScore();
		}
		return score;
	}

	public String toString() {
		return String.format("result,%s,%s,%s", player.getId(), test.getId(), duration);
	}

	public Question nextQuestion() {
		Question question = null;
		if (answers.size() < test.getQuestions().size()) {
			question = test.getQuestions().get(answers.size());
		}
		return question;
	}

	public boolean hasNextQuestion() {
		return answers.size() < test.getQuestions().size();
	}
}
