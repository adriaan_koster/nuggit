
public class StraightPath implements Path {

    private Orientation orientation = Orientation.NORTH;

    @Override
    public Orientation[] exits() {
        if (orientation == Orientation.NORTH) {
            return new Orientation[] { Orientation.NORTH, Orientation.SOUTH };
        } else {
            return new Orientation[] { Orientation.EAST, Orientation.WEST };
        }
    }

    @Override
    public Orientation[] getPossibleOrientations() {
        return new Orientation[] { Orientation.NORTH, Orientation.EAST };
    }

    @Override
    public void setOrientation(Orientation orientation) {
        if (orientation == Orientation.NORTH || orientation == Orientation.SOUTH) {
            this.orientation = Orientation.NORTH;
        }
        else {
            this.orientation = Orientation.EAST;
        }
    }
}
