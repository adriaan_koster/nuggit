
public class CornerPath implements Path {

    private Orientation orientation = Orientation.NORTH;

    @Override
    public Orientation[] exits() {
        if (orientation == Orientation.NORTH) {
            return new Orientation[] { Orientation.NORTH, Orientation.EAST };
        } else if (orientation == Orientation.EAST) {
            return new Orientation[] { Orientation.EAST, Orientation.SOUTH };
        } else if (orientation == Orientation.SOUTH) {
            return new Orientation[] { Orientation.SOUTH, Orientation.WEST };
        } else {
            return new Orientation[] { Orientation.WEST, Orientation.NORTH };
        }
    }

    @Override
    public Orientation[] getPossibleOrientations() {
        return new Orientation[] { Orientation.NORTH, Orientation.EAST, Orientation.SOUTH, Orientation.WEST };
    }

    @Override
    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }
}
