public interface Path {
    Orientation[] getPossibleOrientations();
    void setOrientation(Orientation orientation);
    Orientation[] exits();
}

