package nl.nuggit.evolution;

public class Individual {
    private String dna;

    public Individual(String dna) {
        this.dna = dna;
    }

    public String getDna() {
        return dna;
    }

    public void setDna(String dna) {
        this.dna = dna;
    }

    @Override
    public String toString() {
        return dna;
    }
}
