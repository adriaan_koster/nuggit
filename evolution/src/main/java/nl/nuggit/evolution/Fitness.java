package nl.nuggit.evolution;

public interface Fitness {

    /**
     * Determines the fitness value of the individual on a scale from 0 to 100
     * 
     * @param individual the individual
     * @return the fitness value
     */
    public double evaluate(Individual individual);
}
