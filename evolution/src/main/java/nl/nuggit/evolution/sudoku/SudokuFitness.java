package nl.nuggit.evolution.sudoku;

import java.util.HashSet;
import java.util.Set;

import nl.nuggit.evolution.Fitness;
import nl.nuggit.evolution.Individual;

public class SudokuFitness implements Fitness {

    public double evaluate(Individual individual) {
        if (individual == null) {
            return 0;
        }
        Board board = new Board(individual.getDna());
        int filledSquares = 0;
        boolean valid = true;
        outer: for (Area area : board.getAreas()) {
            Set<Integer> values = new HashSet<Integer>();
            for (Square square : area.getSquares()) {
                if (square.getValue() > 0) {
                    filledSquares++;
                    if (values.contains(square.getValue())) {
                        valid = false;
                        break outer;
                    }
                    values.add(square.getValue());
                }
            }
        }
        if (valid) {
            return (filledSquares / 91) * 100.0;
        }
        else {
            return 0;
        }
    }

}
