package nl.nuggit.evolution.sudoku;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nl.nuggit.evolution.sudoku.Area.Shape;

/**
 * Representation of a board.
 */
public class Board {

    private static final List<Integer> ALL_VALUES = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
    private Square[][] values = new Square[9][9];
    Set<Square> squares = new HashSet<Square>();
    Area[] rows = new Area[9];
    Area[] columns = new Area[9];
    Area[] blocks = new Area[9];

    public Board(String state) {
        if (state == null) {
            throw new IllegalArgumentException("State cannot be null");
        }
        if (state.length() < 81) {
            throw new IllegalArgumentException("State length should be at least 81 characters");
        }
        init(state);
    }

    private void init(String state) {
        int x = 0;
        int y = 0;
        for (String str : state.split("")) {
            if (str.length() == 0) {
                continue;
            }
            addSquare(x, y, Integer.parseInt(str));
            x++;
            if (x > 8) {
                x = 0;
                y++;
                if (y > 8) {
                    break;
                }
            }
        }
        initCandidates();
    }

    private void addSquare(int x, int y, int value) {
        Square square = new Square(x, y, value, this);
        values[x][y] = square;
        squares.add(square);
        Area row = getRow(square);
        if (row == null) {
            row = new Area(Shape.ROW);
            rows[square.getY()] = row;
        }
        row.add(square);
        Area column = getColumn(square);
        if (column == null) {
            column = new Area(Shape.COLUMN);
            columns[square.getX()] = column;
        }
        column.add(square);
        Area block = getBlock(square);
        if (block == null) {
            block = new Area(Shape.BLOCK);
            blocks[square.getBlockIndex()] = block;
        }
        block.add(square);
    }

    private void initCandidates() {
        for (Square square : squares) {
            if (square.isEmpty()) {
                square.getCandidates().addAll(ALL_VALUES);
                for (Area area : getAreas(square)) {
                    for (Square neighbor : area.getSquares()) {
                        square.getCandidates().remove(neighbor.getValue());
                    }
                }
            }
        }
    }

    public void updateCandidates(Square square) {
        for (Area area : getAreas(square)) {
            for (Square neighbor : area.getSquares()) {
                neighbor.getCandidates().remove(square.getValue());
            }
        }
    }

    public boolean isSolved() {
        int filled = 0;
        for (Square square : squares) {
            if (!square.isEmpty()) {
                filled++;
            }
        }
        return filled == 81;
    }
    
    public Set<Square> getSquares() {
        return squares;
    }

    public Area getBlock(Square square) {
        return blocks[square.getBlockIndex()];
    }

    public Set<Area> getBlocks() {
        return new HashSet<Area>(Arrays.asList(blocks));
    }

    public Area getRow(Square square) {
        return rows[square.getY()];
    }

    public Set<Area> getRows() {
        return new HashSet<Area>(Arrays.asList(rows));
    }

    public Area getColumn(Square square) {
        return columns[square.getX()];
    }

    public Set<Area> getColumns() {
        return new HashSet<Area>(Arrays.asList(columns));
    }

    public Set<Area> getAreas() {
        Set<Area> result = new HashSet<Area>();
        result.addAll(getBlocks());
        result.addAll(getRows());
        result.addAll(getColumns());
        return result;
    }

    public Set<Area> getAreas(Square square) {
        Set<Area> result = new HashSet<Area>();
        result.add(getBlock(square));
        result.add(getRow(square));
        result.add(getColumn(square));
        return result;
    }

    public Square getSquare(int x, int y) {
        return values[x][y];
    }
}
