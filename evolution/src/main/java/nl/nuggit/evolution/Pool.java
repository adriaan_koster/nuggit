package nl.nuggit.evolution;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Pool {

    private int maxSize;
    private TreeSet<Individual> individuals;
    private Fitness fitness;

    public Pool(int maxSize, Fitness fitness) {
        this.maxSize = maxSize;
        this.individuals = new TreeSet<Individual>(new IndividualComparator());
        this.fitness = fitness;
    }

    public Set<Individual> getIndividuals() {
        return individuals;
    }

    public void add(Individual individual) {
        individuals.add(individual);
        if (individuals.size() > maxSize) {
            individuals.remove(individuals.last());
        }
    }

    public boolean isFull() {
        return individuals.size() >= maxSize;
    }

    public class IndividualComparator implements Comparator<Individual> {
        public int compare(Individual individual1, Individual individual2) {
            Double fitness1 = fitness.evaluate(individual1);
            Double fitness2 = fitness.evaluate(individual2);
            return fitness2.compareTo(fitness1);
        }
    }

    @Override
    public String toString() {
        String eol = System.getProperty("line.separator");
        StringBuilder sb = new StringBuilder();
        sb.append("Pool size=").append(individuals.size()).append(eol);
        for (Individual individual : individuals) {
            sb.append(individual.toString()).append(eol);
            sb.append(" fitness=").append(fitness.evaluate(individual)).append(eol);
        }
        return sb.toString();
    }
}
