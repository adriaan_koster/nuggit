package nl.nuggit.filemap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * HashMap extension which stores all entries as files.
 */
public class FileMap extends HashMap<String, Serializable> {

    private static final long serialVersionUID = 1L;
    private File rootDirectory;

    /**
     * @param rootDirectory the directory in which all entries are stored
     */
    public FileMap(File rootDirectory) {
        this.rootDirectory = rootDirectory;
        if (rootDirectory == null) {
            throw new IllegalArgumentException("root directory cannot be null");
        }
        if (!rootDirectory.exists()) {
            rootDirectory.mkdirs();
        }
        if (!rootDirectory.isDirectory()) {
            throw new IllegalArgumentException("Not a directory: " + rootDirectory);
        }
        for (File file : rootDirectory.listFiles()) {
            if (file.isFile()) {
                super.put(file.getName(), read(file));
            }
        }
    }

    private static Serializable read(File file) {

        ObjectInputStream in = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            in = new ObjectInputStream(fis);
            return (Serializable) in.readObject();
        }
        catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Could not read file", e);
        }
        catch (IOException e) {
            throw new IllegalArgumentException("Could not read from file", e);
        }
        catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("Could not read value", e);
        }
        finally {
            if (in != null) {
                try {
                    in.close();
                }
                catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public void clear() {
        for (File file : rootDirectory.listFiles()) {
            if (file.isFile()) {
                file.delete();
            }
        }
        super.clear();
    }

    /**
     * @throws UnsupportedOperationException The clone operation is not supported by this implementation.
     */
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("The clone operation is not supported by this implementation.");
    }

    @Override
    public Serializable put(String key, Serializable value) {
        Serializable oldValue = super.put(key, value);
        write(key, value);
        return oldValue;
    }

    @Override
    public void putAll(Map<? extends String, ? extends Serializable> map) {
        super.putAll(map);
        for (Map.Entry<? extends String, ? extends Serializable> entry : map.entrySet()) {
            write(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public Serializable remove(Object key) {
        File file = new File(rootDirectory, getFileName(key));
        if (!file.isDirectory() && file.exists()) {
            file.delete();
        }
        return super.remove(key);
    }

    private void write(String key, Serializable value) {

        String filename = getFileName(key);
        ObjectOutputStream oos = null;
        try {
            File file = new File(rootDirectory, filename);
            FileOutputStream fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(value);
        }
        catch (IOException e) {
            throw new IllegalArgumentException("Could not write", e);
        }
        finally {
            if (oos != null) {
                try {
                    oos.close();
                }
                catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    private String getFileName(Object key) {
        if (key == null) {
            throw new IllegalArgumentException("Keys cannot be null");
        }
        String filename = String.valueOf(key);
        if (filename.length() == 0) {
            throw new IllegalArgumentException("Keys cannot be empty");
        }
        return filename;
    }

}
