package nl.nuggit.filemap;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

public class App {

    public static void main(String[] args) {

        FileMap fileMap = new FileMap(new File("filemap-root"));

        System.out.println("Existing data:");
        for (Entry<String, Serializable> entry : fileMap.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }

        fileMap.clear();
        
        List<String> value1 = new ArrayList<String>();
        value1.add("Hello");
        value1.add("Kitty");
        fileMap.put("greeting", (Serializable) value1);
        
        fileMap.put("aaaaa", 872364876L);
        
        fileMap.put("bbbbb", new IllegalArgumentException("dummy"));
        fileMap.remove("bbbbb");
        
        fileMap.put("today", new Date());
    }
}
