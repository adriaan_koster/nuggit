package nl.nuggit.webtail;

import org.apache.commons.io.input.ReversedLinesFileReader;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet which displays the last lines of a file.
 */
public class WebTail extends HttpServlet {

    private static final String COOKIE_NAME = "webtail";
    public static final int COOKIE_MAX_AGE = 60 * 60 * 24 * 365;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Settings settings = new Settings();

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (COOKIE_NAME.equals(cookie.getName())) {
                    settings.setCookieValue(cookie.getValue());
                }
            }
        }

        boolean settingsChanged = settings.setRequestParameters(request);
        if (settingsChanged) {
            Cookie cookie = new Cookie(COOKIE_NAME, settings.toCookieValue());
            cookie.setMaxAge(COOKIE_MAX_AGE);
            response.addCookie(cookie);
        }

        PrintWriter out = response.getWriter();

        String[] tail = new String[settings.getLines()];
        File tailFile = settings.getFile();
        if (tailFile.exists()) {
            ReversedLinesFileReader reader = new ReversedLinesFileReader(tailFile);
            for (int i = tail.length - 1; i > 0; i--) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                tail[i] = line;
            }
        } else {
            System.err.printf("File %s does not exist\n", tailFile);
        }

        out.printf("<HTML>");
        out.printf("<HEAD><META HTTP-EQUIV=\"refresh\" CONTENT=\"%s\"></HEAD>", settings.getIntervalSeconds());
        out.printf("<BODY>");
        out.printf("<H1>WebTail</H1>");
        out.printf("<HR/>");
        out.printf("<UL>");
        out.printf("<LI>%s = %s</LI>", Settings.PROPERTY_FILE, settings.getFile());
        out.printf("<LI>%s = %s</LI>", Settings.PROPERTY_LINES, settings.getLines());
        out.printf("<LI>%s = %s</LI>", Settings.PROPERTY_INTERVAL_SECONDS, settings.getIntervalSeconds());
        out.printf("</UL>");
        out.printf("<HR/>");
        out.printf("<PRE>");
        for (String line : tail) {
            if (line != null) {
                out.println(line);
            }
        }
        out.printf("</PRE>");
        out.println("</BODY>");
        out.println("</HTML>");
        out.flush();
        out.close();
    }
}
