package nl.nuggit.webtail;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

public class Settings {
    public final static String PROPERTY_FILE = "file";
    public final static String PROPERTY_LINES = "lines";
    public final static String PROPERTY_INTERVAL_SECONDS = "intervalSeconds";

    private final static String PROPERTY_FILE_DEFAULT = "/var/log/tomcat7/catalina.out";
    private final static int PROPERTY_LINES_DEFAULT = 40;
    private final static int PROPERTY_INTERVAL_SECONDS_DEFAULT = 10;

    private String file = PROPERTY_FILE_DEFAULT;
    private int lines = PROPERTY_LINES_DEFAULT;
    private int intervalSeconds = PROPERTY_INTERVAL_SECONDS_DEFAULT;

    public File getFile() {
        return new File(file);
    }

    public void setFile(String fileProperty) {
        if (fileProperty != null && fileProperty.length() > 0) {
            file = fileProperty;
        }
    }

    public int getLines() {
        return lines;
    }

    public void setLines(String linesProperty) {
        try {
            lines = Integer.parseInt(linesProperty);
        } catch (NumberFormatException ignore) {
        }
    }

    public int getIntervalSeconds() {
        return intervalSeconds;
    }

    public void setIntervalSeconds(String intervalProperty) {
        try {
            intervalSeconds = Integer.parseInt(intervalProperty);
        } catch (NumberFormatException ignore) {
        }
    }

    public void setCookieValue(String cookieValue) {
        if (cookieValue != null) {
            String[] values = cookieValue.split(",");
            if (values.length == 3) {
                setFile(values[0]);
                setLines(values[1]);
                setIntervalSeconds(values[2]);
            }
        }
    }

    public boolean setRequestParameters(HttpServletRequest request) {
        boolean changed = false;
        String oldFile = file;
        setFile(request.getParameter(PROPERTY_FILE));
        if (!oldFile.equals(file)) {
            changed = true;
        }
        int oldLines = lines;
        setLines(request.getParameter(PROPERTY_LINES));
        if (oldLines != lines) {
            changed = true;
        }
        int oldInterval = intervalSeconds;
        setIntervalSeconds(request.getParameter(PROPERTY_INTERVAL_SECONDS));
        if (oldInterval != intervalSeconds) {
            changed = true;
        }
        return changed;
    }

    public String toCookieValue() {
        return String.format("%s,%s,%s", file, lines, intervalSeconds);
    }
}
