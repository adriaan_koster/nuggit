<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

	<h1>Home</h1>

	<p>
		Welcome
		<c:out value="${context.principal}" />
		!
	</p>

	<a href="products">View products</a>
	
</body>
</html>