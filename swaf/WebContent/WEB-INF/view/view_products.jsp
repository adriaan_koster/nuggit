<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

	<h1>View products</h1>

	<ul>
		<c:forEach items="${productlist}" var="product">
			<li><c:out value="${product.description}" /> - <c:out
					value="${product.price}" /></li>
		</c:forEach>
	</ul>

	<a href="home">Home</a>
</body>
</html>
