<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

	<h1>Login</h1>

	<c:choose>
		<c:when test="${!context eq null}">
			User <c:out value="${context.principal}" /> is already logged in.
		</c:when>
		<c:otherwise>
			<form method="POST">
				<table>
					<tr>
						<td>Username :</td>
						<td><input name="username" size=15 type="text" />
						</td>
					</tr>
					<tr>
						<td>Password :</td>
						<td><input name="password" size=15 type="text" />
						</td>
					</tr>
				</table>
				<input type="submit" value="login" />
			</form>
		</c:otherwise>
	</c:choose>

</body>
</html>