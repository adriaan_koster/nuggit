package nl.nuggit.swaf.context;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import nl.nuggit.swaf.model.Context;
import nl.nuggit.swaf.model.Permission;
import nl.nuggit.swaf.service.AuthenticationService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


public class ContextStore {

    public static final String NAME = "CONTEXT_STORE";
    private static final Logger LOG = Logger.getLogger(ContextStore.class);

    private final Map<String, Context> contexts = new HashMap<String, Context>();
    @Autowired
    private AuthenticationService authenticationService;

    public Context authenticate(String... credentials) {
        Context context;
        String principal = credentials[0];
        context = findPrincipal(principal);
        if (context == null) {
            boolean isAuthenticated = authenticationService.authenticate(principal, credentials[1]);
            if (isAuthenticated) {
                String token = UUID.randomUUID().toString();
                context = new Context(token, principal);
                context.grant(Permission.ACCESS);
                contexts.put(token, context);
                LOG.debug(String.format("Added principal %s to context store", principal));
            }
        }
        else {
            LOG.debug(String.format("Principal %s already authenticated", principal));
        }
        return context;
    }

    private Context findPrincipal(String principal) {
        for (Context context : contexts.values()) {
            if (context.getPrincipal().equals(principal)) {
                return context;
            }
        }
        return null;
    }

    public Context read(String token) {
        Context context = contexts.get(token);
        LOG.debug(String.format("Found context %s for token %s", context, token));
        return context;
    }

    public boolean delete(String token) {
        boolean success = contexts.remove(token) != null;
        LOG.debug(String.format("Removal of context for token %s success=", token, success));
        return success;
    }
}
