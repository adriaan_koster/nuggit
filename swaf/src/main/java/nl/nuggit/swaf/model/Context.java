package nl.nuggit.swaf.model;

import java.util.HashSet;
import java.util.Set;

public class Context {

    public static String NAME = "context";

    private final String token;
    private final String principal;
    private final Set<Permission> permissions = new HashSet<Permission>();

    public Context(String token, String principal) {
        this.token = token;
        this.principal = principal;
    }

    public String getToken() {
        return token;
    }

    public String getPrincipal() {
        return principal;
    }

    public boolean grant(Permission permission) {
        return permissions.add(permission);
    }

    public boolean revoke(Permission permission) {
        return permissions.remove(permission);
    }

    public boolean has(Permission permission) {
        return permissions.contains(permission);
    }

    @Override
    public String toString() {
        return principal;
    }
}
