package nl.nuggit.swaf.model;

import java.util.HashMap;
import java.util.Map;

public class ViewData {

    private String view;
    private final Map<String, Object> data = new HashMap<String, Object>();

    public ViewData() {
        view = null;
    }

    public ViewData(String view) {
        this.view = view;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public Map<String, Object> getData() {
        return data;
    }
}
