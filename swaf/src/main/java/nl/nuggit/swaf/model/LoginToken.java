package nl.nuggit.swaf.model;

public class LoginToken {
    public static final String NAME = "LOGIN_TOKEN";
    public static final int EXPIRY_SECONDS = 15 * 60;
}
