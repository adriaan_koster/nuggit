package nl.nuggit.swaf.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.nuggit.swaf.model.LoginToken;

import org.apache.log4j.Logger;


public class LoginTokenManager {

    private static final Logger LOG = Logger.getLogger(LoginTokenManager.class);

    public static String getToken(HttpServletRequest request) {
        String token = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (LoginToken.NAME.equals(cookie.getName())) {
                    token = cookie.getValue();
                    break;
                }
            }
        }
        LOG.debug(String.format("Got token %s", token));
        return token;
    }

    public static void setToken(String token, HttpServletResponse response) {
        Cookie userCookie = new Cookie(LoginToken.NAME, token);
        userCookie.setMaxAge(LoginToken.EXPIRY_SECONDS);
        response.addCookie(userCookie);
        LOG.debug(String.format("Set token %s", token));
    }
}
