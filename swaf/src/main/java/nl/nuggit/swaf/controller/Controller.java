package nl.nuggit.swaf.controller;

import java.io.IOException;
import java.util.Map.Entry;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.nuggit.swaf.context.ContextStore;
import nl.nuggit.swaf.interaction.Interaction;
import nl.nuggit.swaf.interaction.InteractionFactory;
import nl.nuggit.swaf.model.Context;
import nl.nuggit.swaf.model.ViewData;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.HttpRequestHandler;


public class Controller implements HttpRequestHandler {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Controller.class);

    @Autowired
    private ContextStore contextStore;
    @Autowired
    private InteractionFactory interactionFactory;

    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Context context = getContext(request);
        Interaction interaction = interactionFactory.create(context, request.getPathInfo());
        if (interaction != null) {
            ViewData view = interaction.execute(request, response);
            forwardToView(request, response, view);
        }
    }

    private Context getContext(HttpServletRequest request) {
        String token = LoginTokenManager.getToken(request);
        Context context = contextStore.read(token);
        request.setAttribute(Context.NAME, context);
        return context;
    }

    private void forwardToView(HttpServletRequest request, HttpServletResponse response, ViewData view) throws ServletException, IOException {
        LOG.debug(String.format("Forwarding to view %s", view.getView()));
        for (Entry<String, Object> entry : view.getData().entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            request.setAttribute(key, value);
            LOG.debug(String.format("Added attribute %s = %s", key, value));
        }
        RequestDispatcher rd = request.getRequestDispatcher(view.getView());
        rd.forward(request, response);
    }
}
