package nl.nuggit.swaf.application.interaction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.nuggit.swaf.context.ContextStore;
import nl.nuggit.swaf.controller.LoginTokenManager;
import nl.nuggit.swaf.interaction.Interaction;
import nl.nuggit.swaf.model.Context;
import nl.nuggit.swaf.model.ViewData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public class Login extends Interaction {

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    @Autowired
    private ContextStore contextStore;
    @Autowired
    @Qualifier("loginPage")
    private String loginPage;
    @Autowired
    @Qualifier("homePage")
    private String homePage;

    public Login(String name) {
        super(name);
    }

    public ViewData execute(HttpServletRequest request, HttpServletResponse response) {
        Context context = (Context) request.getAttribute(Context.NAME);
        if (context == null) {
            String username = request.getParameter(USERNAME);
            String password = request.getParameter(PASSWORD);
            if (username == null || password == null) {
                return new ViewData(loginPage);
            }
            context = contextStore.authenticate(username, password);
            LoginTokenManager.setToken(context.getToken(), response);
            request.setAttribute(Context.NAME, context);
        }
        return new ViewData(homePage);
    }
}
