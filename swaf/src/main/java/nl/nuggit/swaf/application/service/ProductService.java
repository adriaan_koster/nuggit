package nl.nuggit.swaf.application.service;

import java.util.List;

import nl.nuggit.swaf.application.model.Product;


public interface ProductService {
    public List<Product> listAll();
}
