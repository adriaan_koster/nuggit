package nl.nuggit.swaf.application.service.impl;

import nl.nuggit.swaf.service.AuthenticationService;

import org.apache.log4j.Logger;


public class AuthenticationServiceImpl implements AuthenticationService {

    private static final Logger LOG = Logger.getLogger(AuthenticationServiceImpl.class);

    public boolean authenticate(String username, String password) {
        LOG.debug(String.format("Authentication requested for username %s ", username));
        return true;
    }
}
