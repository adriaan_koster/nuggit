package nl.nuggit.swaf.application.interaction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.nuggit.swaf.application.service.ProductService;
import nl.nuggit.swaf.interaction.Interaction;
import nl.nuggit.swaf.model.ViewData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public class ViewProducts extends Interaction {

    private static final String PRODUCTLIST = "productlist";

    @Autowired
    private ProductService productService;
    @Autowired
    @Qualifier("viewProductsPage")
    private String viewProductsPage;

    public ViewProducts(String name) {
        super(name);
    }

    @Override
    public ViewData execute(HttpServletRequest request, HttpServletResponse response) {
        ViewData result = new ViewData(viewProductsPage);
        result.getData().put(PRODUCTLIST, productService.listAll());
        return result;
    }
}
