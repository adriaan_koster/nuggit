package nl.nuggit.swaf.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import nl.nuggit.swaf.application.model.Product;
import nl.nuggit.swaf.application.service.ProductService;


public class ProductServiceImpl implements ProductService {

    public List<Product> listAll() {
        List<Product> products = new ArrayList<Product>();
        products.add(new Product(1L, "Juicy apple", 0.40));
        products.add(new Product(2L, "Fresh orange", 0.30));
        products.add(new Product(3L, "Yellow banana", 0.55));
        products.add(new Product(4L, "Delicious grapes", 0.50));
        products.add(new Product(5L, "Pink Peach", 0.45));
        return products;
    }

}
