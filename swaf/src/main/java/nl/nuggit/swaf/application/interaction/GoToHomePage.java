package nl.nuggit.swaf.application.interaction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.nuggit.swaf.interaction.Interaction;
import nl.nuggit.swaf.model.ViewData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public class GoToHomePage extends Interaction {

    @Autowired
    @Qualifier("homePage")
    private String homePage;

    public GoToHomePage(String name) {
        super(name);
    }

    @Override
    public ViewData execute(HttpServletRequest request, HttpServletResponse response) {
        return new ViewData(homePage);
    }
}
