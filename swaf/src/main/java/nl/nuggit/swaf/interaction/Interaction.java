package nl.nuggit.swaf.interaction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.nuggit.swaf.model.ViewData;


public abstract class Interaction {

    public static final String NAME = "interaction";
    private final String name;

    public Interaction() {
        this.name = this.getClass().getSimpleName();
    }
    
    public Interaction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract ViewData execute(HttpServletRequest request, HttpServletResponse response);

}
