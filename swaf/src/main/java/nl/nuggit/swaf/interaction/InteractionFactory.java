package nl.nuggit.swaf.interaction;

import java.util.List;

import nl.nuggit.swaf.model.Context;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public class InteractionFactory {

    private static final Logger LOG = Logger.getLogger(InteractionFactory.class);

    @Autowired
    private List<Interaction> interactions;

    @Autowired
    @Qualifier("loginInteraction")
    private Interaction loginInteraction;

    public Interaction create(Context context, String pathInfo) {

        Interaction result = null;
        if (context == null) {
            result = loginInteraction;
        }
        else {
            String interactionName = pathInfo;
            if (interactionName != null) {
                interactionName = interactionName.substring(1);
            }
            LOG.debug(String.format("interactionName=%s", interactionName));
            result = lookUp(interactionName);
            if (result == null) {
                LOG.debug(String.format("interactionName %s not found", interactionName));
            }
        }
        return result;
    }

    private Interaction lookUp(String name) {
        Interaction result = null;
        for (Interaction interaction : interactions) {
            if (interaction.getName().equals(name)) {
                result = interaction;
                break;
            }
        }
        return result;
    }
}
