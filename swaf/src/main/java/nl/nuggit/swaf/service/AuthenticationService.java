package nl.nuggit.swaf.service;

public interface AuthenticationService {
    public boolean authenticate(String username, String password);
}
