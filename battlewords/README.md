# Battlewords

A crossover game between battleships and hangman. 

Both players use a fixed set of words. 
Each player hides the words on their grid.
Then players take turns to guess a coordinate on the other player's grid. 
The other player responds with the letter on the given coordindate, or 'miss'
If all the letters of a word have been guessed, the other player also names the guessed word.
A player may continue guessing until a miss occurs, then the turn goes to the other player
The first one to find all the words of the other player has won