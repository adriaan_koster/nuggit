package nl.nuggit.battlewords;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import nl.nuggit.battlewords.config.Media;
import nl.nuggit.battlewords.config.Props;

public class App extends Application {

    public static Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {

        primaryStage = stage;

        HBox root = new HBox();
        Scene scene = new Scene(root, 1400, 800);

        GridPane grid = createGrid();
        addTiles(grid);
        HBox messagesBox = createMessagesBox();
        VBox leftPane = new VBox();
        leftPane.getChildren().addAll(grid, messagesBox);

        StackPane rightPane = new StackPane();
        root.getChildren().addAll(leftPane, rightPane);

        stage.setTitle("Battle Words!");
        scene.getStylesheets().add("stylesheet.css");
        stage.setScene(scene);
        stage.setFullScreen(Props.fullscreen());
        stage.show();

        stage.setOnCloseRequest(e -> {
            Platform.exit();
        });
    }

    private void addTiles(GridPane grid)   {
        Tile tile = createTile(grid, 1, 1);
    };

    private Tile createTile(GridPane grid, int col, int row) {
        Tile tile = new Tile();
        tile.setOnMouseClicked(t -> {
            isClicked(col, row);
        });
        grid.add(tile, col, row);
        return tile;
    }

    private void isClicked(int col, int row) {
        System.out.printf("Tile %s, %s was clicked%n", col, row);

    }

    private VBox createPane(StackPane rightPane) {
        VBox pane = new VBox(10.0);
        pane.setVisible(false);
        rightPane.getChildren().add(pane);
        return pane;
    }

    private GridPane createGrid() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10.0);
        grid.setVgap(10.0);
        grid.setPadding(new Insets(10, 10, 10, 10));
        return grid;
    }

    private HBox createMessagesBox() {
        HBox messagesBox = new HBox();
        messagesBox.setAlignment(Pos.CENTER);
        return messagesBox;
    }

    private HBox createInputBox() {
        TextField input = new TextField();
        input.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                handleInput(input);
            }
        });
        input.setPrefColumnCount(10);
        Button submit = new Button();
        submit.setText("Enter");
        submit.setOnAction(event -> handleInput(input));
        HBox inputBox = new HBox();
        inputBox.setAlignment(Pos.CENTER);
        inputBox.getChildren().addAll(input, submit);
        return inputBox;
    }

    private void handleInput(TextField input) {
        Media.enter().play();
        String text = input.getText();
        System.out.printf("Received input %s%n", text);
        input.clear();
    }

}