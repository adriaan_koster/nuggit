package nl.nuggit.battlewords.config;

import java.util.Properties;

public class TypedProperties {

    private final Properties properties;

    public TypedProperties(Properties properties) {
        this.properties = properties;
    }

    public String getString(String key) {
        return System.getProperty(key, properties.getProperty(key));
    }

    public boolean getBoolean(String key) {
        return Boolean.parseBoolean(getString(key));
    }

    public int getInt(String key) {
        return Integer.parseInt(getString(key));
    }
}
