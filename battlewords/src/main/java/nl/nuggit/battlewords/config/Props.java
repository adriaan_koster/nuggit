package nl.nuggit.battlewords.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Props {

    private static TypedProperties PROPERTIES;

    static {
        try {
            PROPERTIES = readProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static TypedProperties readProperties() throws IOException {
        Properties properties = new Properties();
        String propertiesPath = "/app.properties";
        InputStream inputStream = Props.class.getResourceAsStream(propertiesPath);
        if (inputStream == null) {
            throw new RuntimeException(String.format("Could not read properties from classpath: %s", propertiesPath));
        }
        properties.load(inputStream);
        return new TypedProperties(properties);
    }

    public static boolean persistance() {
        return PROPERTIES.getBoolean("persistance");
    }

    public static boolean fullscreen() {
        return PROPERTIES.getBoolean("fullscreen");
    }

    public static int coolDownInitialSeconds() {
        return PROPERTIES.getInt("cooldown.initial.seconds");
    }

    public static int coolDownPenaltySeconds() {
        return PROPERTIES.getInt("cooldown.penalty.seconds");
    }

    public static int coolDownMaxSeconds() {
        return PROPERTIES.getInt("cooldown.max.seconds");
    }

    public static String socketHost() {
        return PROPERTIES.getString("socket.host");
    }

    public static boolean emailEnabled() {
        return PROPERTIES.getBoolean("email.enabled");
    }

    public static String emailUser() {
        return PROPERTIES.getString("email.user");
    }

    public static String emailPassword() {
        return PROPERTIES.getString("email.password");
    }

    public static int socketPort() {
        return PROPERTIES.getInt("socket.port");
    }

    public static int blackHoleWarRounds() {
        return PROPERTIES.getInt("black-hole-war.max.rounds");
    }

}
