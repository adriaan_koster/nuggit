package nl.nuggit.battlewords.config;

import java.util.Timer;
import java.util.TimerTask;

import javafx.scene.media.MediaPlayer;

public class Media {

    private static MediaPlayer wrongAnswer;
    private static MediaPlayer hintGiven;
    private static MediaPlayer rightAnswer;
    private static MediaPlayer maffeMedleySong;
    private static MediaPlayer maffeMedleySong2;
    private static MediaPlayer wonkaLaughing;
    private static MediaPlayer coinFlipSound;
    private static MediaPlayer select;
    private static MediaPlayer starship;
    private static MediaPlayer howl;
    private static Timer starShipTimer;
    private static Timer howlTimer;
    private static MediaPlayer brig;
    private static Timer brigTimer;
    private static MediaPlayer credits;

    public static void startNevelenBackground() {
        starShipTimer = new Timer();
        starShipTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                starship().play();
            }
        }, 0, 116000);

        howlTimer = new Timer();
        howlTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                howl().play();
            }
        }, 12000, 92000);
    }

    private static void stopNevelenBackgound() {
        if (starship != null) {
            starship.stop();
            starShipTimer.cancel();
        }
        if (howl != null) {
            howl.stop();
            howlTimer.cancel();
        }
    }

    public static MediaPlayer starship() {
        starship = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/starship.mp3").toString()));
        starship.setVolume(0.6);
        return starship;
    }

    public static MediaPlayer howl() {
        howl = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/howl.mp3").toString()));
        howl.setVolume(0.6);
        return howl;
    }

    public static void startCoinGameBackground() {
        brigTimer = new Timer();
        brigTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                brig().play();
            }
        }, 0, 82000);
    }

    public static void stopCoinGameBackground() {
        if (brig != null) {
            brig.stop();
            brigTimer.cancel();
        }
    }

    public static MediaPlayer brig() {
        brig = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/tos_forcefield_1.mp3").toString()));
        brig.setVolume(0.6);
        return brig;
    }

    public static MediaPlayer coinFlip() {
        coinFlipSound = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/coin_flip.wav").toString()));
        return coinFlipSound;
    }

    public static MediaPlayer select() {
        select = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/communications_start_transmission.mp3").toString()));
        return select;
    }

    public static MediaPlayer enter() {
        select = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/xindi_controls02.mp3").toString()));
        return select;
    }

    public static MediaPlayer wrongAnswer() {
        wrongAnswer = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/denybeep3.mp3").toString()));
        return wrongAnswer;
    }

    public static MediaPlayer hintGiven() {
        hintGiven = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/forcefield_off.mp3").toString()));
        return hintGiven;
    }

    public static MediaPlayer rightAnswer() {
        rightAnswer = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/input_ok_3_clean.mp3").toString()));
        return rightAnswer;
    }

    public static MediaPlayer maffeMedleySong() {
        maffeMedleySong = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/maffe_medley.mp3").toString()));
        return maffeMedleySong;
    }

    public static MediaPlayer maffeMedleySong2() {
        maffeMedleySong2 = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/maffe_medley2.mp3").toString()));
        return maffeMedleySong2;
    }

    public static MediaPlayer scaryWonka() {
        wonkaLaughing = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/willy_wonka_laughing.mp4").toString()));
        wonkaLaughing.setAutoPlay(true);
        return wonkaLaughing;
    }

    public static MediaPlayer escapeAndCredits() {
        credits = new MediaPlayer(new javafx.scene.media.Media(Media.class.getResource("/media/nevelen_der_tijd_credits_v2.mp4").toString()));
        credits.setAutoPlay(true);
        return credits;
    }

    public static void stopAll() {
        stopNevelenBackgound();
        stopCoinGameBackground();
        if (maffeMedleySong != null) {
            maffeMedleySong.stop();
        }
        if (wonkaLaughing != null) {
            wonkaLaughing.stop();
        }
        if (credits != null) {
            credits.stop();
        }
    }
}
