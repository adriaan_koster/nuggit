package nl.nuggit.battlewords;

import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class Tile extends StackPane {

    private static final int SQUARE_SIZE = 20;

    private Rectangle rectangle = new Rectangle(SQUARE_SIZE, SQUARE_SIZE);
    private Text text = new Text();

    public Tile() {
        getChildren().addAll(rectangle, text);
    }

    public Text getText() {
        return text;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }
}
