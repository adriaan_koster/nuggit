package nl.nuggit.keezen;

public class Settings {
    public static final int CARDS_PER_TYPE = 4;
    public static final int NUMBER_OF_PLAYERS = 4;
    public static final int POSITIONS_PER_PLAYER = 16;
    public static final int NUMBER_OF_PAWNS = 4;

    public static int playPositions() {
        return NUMBER_OF_PLAYERS * POSITIONS_PER_PLAYER;
    }
}
