package nl.nuggit.keezen;

public class App {

    private Game game;

    public App() {
        game = new Game();
        game.start();
    }

    public static void main(String[] args) {
        App app = new App();
    }
}
