package nl.nuggit.keezen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import nl.nuggit.keezen.model.Board;
import nl.nuggit.keezen.model.card.Card;
import nl.nuggit.keezen.model.Deck;
import nl.nuggit.keezen.model.KeezException;
import nl.nuggit.keezen.model.Player;

public class Game {

    private List<Player> players;
    private int startPlayerIndex;
    private int currentPlayerIndex;
    private Board board;
    private Deck deck;

    public Game() {
        players = initPlayers();
        startPlayerIndex = 0;
        currentPlayerIndex = 0;
        board = new Board(players.stream().map(Player::getPawns).flatMap(i -> i).collect(Collectors.toList()));
        deck = new Deck();
    }

    private List<Player> initPlayers() {
        List<Player> players = new ArrayList<>();
        int playAreaSize = Settings.NUMBER_OF_PLAYERS * Settings.POSITIONS_PER_PLAYER;
        for (int i = 0; i < Settings.NUMBER_OF_PLAYERS; i++) {
            int startPosition = i * Settings.POSITIONS_PER_PLAYER;
            int endPosition = (startPosition + playAreaSize) % playAreaSize;
            players.add(new Player(startPosition, endPosition));
        }
        setPartners(players);
        return players;
    }

    private void setPartners(List<Player> players) {
        int halfPlayerRound = Settings.NUMBER_OF_PLAYERS / 2;
        for (int i = 0; i < halfPlayerRound; i++) {
            Player player = players.get(i);
            Player partner = players.get(i + halfPlayerRound);
            player.setPartner(partner);
            partner.setPartner(player);
        }
    }

    public void start() {

    }

    public void deal(int amountPerPlayer) {
        verifyDeck(amountPerPlayer);
        for (Player player : players) {
            deal(amountPerPlayer, player);
        }
    }

    private void verifyDeck(int amountPerPlayer) {
        int cardsNeeded = amountPerPlayer * players.size();
        if (cardsNeeded > deck.cardsAvailable()) {
            throw new KeezException("Not enough cards available");
        }
    }

    private void deal(int amount, Player player) {
        List<Card> cards = draw(amount);
        player.setCards(cards);
    }

    private List<Card> draw(int amount) {
        List<Card> cards = deck.draw(amount);
        if (cards.size() < amount) {
            throw new KeezException("Ran out of cards during dealing");
        }
        return cards;
    }
}
