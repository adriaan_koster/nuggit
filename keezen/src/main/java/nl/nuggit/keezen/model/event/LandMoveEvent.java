package nl.nuggit.keezen.model.event;

import nl.nuggit.keezen.model.Pawn;

public class LandMoveEvent extends MoveEvent {

    public LandMoveEvent(Pawn actor, int position) {
        super(actor, position);
    }

}
