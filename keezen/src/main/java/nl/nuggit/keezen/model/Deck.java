package nl.nuggit.keezen.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

import nl.nuggit.keezen.Settings;
import nl.nuggit.keezen.model.card.Card;
import nl.nuggit.keezen.model.card.CardType;

public class Deck {

    private List<Card> cardsToDraw;
    private List<Card> discarded;
    private int fullSize;
    private Random random = new Random();
    private boolean reUseDiscards;

    public Deck(boolean reUseDiscards) {
        this();
        this.reUseDiscards = reUseDiscards;
    }

    public Deck() {
        init();
    }

    public void init() {
        cardsToDraw = createCards();
        discarded = new ArrayList<>();
        fullSize = cardsToDraw.size();
    }

    public void reUseDiscards() {
        if (!cardsToDraw.isEmpty()) {
            throw new KeezException("Cannot reuse discards when there are still cards to draw");
        }
        if (discarded.isEmpty()) {
            throw new KeezException("No discarded cards available to reuse");
        }
        cardsToDraw.addAll(discarded);
        discarded.clear();
    }

    private List<Card> createCards() {
        List<Card> cards = new ArrayList<>();
        for (CardType cardType : CardType.values()) {
            add(cardType, Settings.CARDS_PER_TYPE, cards);
        }
        return cards;
    }

    private void add(CardType cardType, int amount, List<Card> cards) {
        for (int i = 0; i < amount; i++) {
            cards.add(new Card(cardType));
        }
    }

    public Optional<Card> draw() {
        if (reUseDiscards && cardsToDraw.isEmpty()) {
            reUseDiscards();
        }
        if (!cardsToDraw.isEmpty()) {
            return Optional.of(cardsToDraw.remove(random.nextInt(cardsToDraw.size())));
        } else {
            return Optional.empty();
        }
    }

    public List<Card> draw(int amount) {
        List<Card> drawn = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            draw().ifPresent(drawn::add);
        }
        return drawn;
    }

    public Stream<Card> getCards() {
        return cardsToDraw.stream();
    }

    public int cardsToDraw() {
        return cardsToDraw.size();
    }

    public int cardsDiscarded() {
        return discarded.size();
    }

    public int cardsAvailable() {
        return cardsToDraw.size() + discarded.size();
    }

    public int fullSize() {
        return fullSize;
    }

    public void discard(Card card) {
        if (cardsToDraw.contains(card)) {
            throw new KeezException("Cannot discard a card before it is drawn");
        }
        if (discarded.contains(card)) {
            throw new KeezException("Cannot discard a card twice");
        }
        discarded.add(card);
    }

}
