package nl.nuggit.keezen.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import nl.nuggit.keezen.Settings;
import nl.nuggit.keezen.model.card.Card;

public class Player {

    private final int startPosition;
    private final int endPosition;
    private final List<Pawn> pawns;
    private List<Card> cards;
    private Player partner;

    public Player(int startPosition, int endPosition) {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.pawns = initPawns();
        cards = new ArrayList<>();
    }

    private List<Pawn> initPawns() {
        List<Pawn> pawns = new ArrayList<>();
        for (int i = 0; i < Settings.NUMBER_OF_PAWNS; i++) {
            pawns.add(new Pawn(this));
        }
        return pawns;
    }

    public Player getPartner() {
        return partner;
    }

    public void setPartner(Player partner) {
        this.partner = partner;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }

    public Stream<Pawn> getPawns() {
        return pawns.stream();
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void discardCard(Card card) {
        if (!cards.remove(card)) {
            throw new KeezException("Player does not have this card to discard");
        }
    }
}
