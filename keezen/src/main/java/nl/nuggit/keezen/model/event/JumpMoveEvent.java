package nl.nuggit.keezen.model.event;

import nl.nuggit.keezen.model.Pawn;

public class JumpMoveEvent extends MoveEvent {

    private Pawn jumped;

    public JumpMoveEvent(Pawn actor, int position, Pawn jumped) {
        super(actor, position);
        this.jumped = jumped;
    }

    public Pawn getJumped() {
        return jumped;
    }
}
