package nl.nuggit.keezen.model;

public class KeezException extends RuntimeException {

    public KeezException(String msg) {
        super(msg);
    }
}
