package nl.nuggit.keezen.model;

public enum Direction {
    FORWARD {
        @Override
        int step(int position) {
            return position + 1;
        }
    }, BACKWARD {
        @Override
        int step(int position) {
            return position - 1;
        }
    };

    abstract int step(int position);
}
