package nl.nuggit.keezen.model;

import java.util.ArrayList;
import java.util.List;

import nl.nuggit.keezen.model.card.Card;
import nl.nuggit.keezen.model.card.CardType;

public class Play {

    private List<Move> moves = new ArrayList<>();

    public Play(Card card, Pawn pawn) {
        switch (card.getCardType()) {
        case START_OR_ONE:
            if (pawn.getArea() == Area.WAIT) {
                pawn.moveToPlayArea();
            } else {
                moves.add(new Move(pawn, 1, Direction.FORWARD));
            }
            break;
        case START:
            if (pawn.getArea() == Area.WAIT) {
                pawn.moveToPlayArea();
            } else {
                throw new KeezException("Start card can only be played for pawns in the START area");
            }
            break;
        case TWELVE:
            moves.add(new Move(pawn, 12, Direction.FORWARD));
            break;
        case SWAP:
            throw new KeezException("Swap card requires two pawns");
        case SEVEN_SPLIT:
            moves.add(new Move(pawn, 7, Direction.FORWARD));
            break;
        case SIX:
            moves.add(new Move(pawn, 6, Direction.FORWARD));
            break;
        case FIVE:
            moves.add(new Move(pawn, 5, Direction.FORWARD));
            break;
        case FOUR_BACK:
            moves.add(new Move(pawn, 4, Direction.BACKWARD));
            break;
        case THREE:
            moves.add(new Move(pawn, 3, Direction.FORWARD));
            break;
        case TWO:
            moves.add(new Move(pawn, 2, Direction.FORWARD));
            break;
        }
    }

    public Play(Card card, Pawn pawn1, int steps1, Pawn pawn2, int steps2) {
        if (card.getCardType() != CardType.SEVEN_SPLIT) {
            throw new KeezException("Only SEVEN_SPLIT can move two pawns");
        }
        if (steps1 + steps2 != 7) {
            throw new KeezException("Total steps of SEVEN_SPLIT must be 7");
        }
        if (!pawnsBelongToSamePlayer(pawn1, pawn2) && !pawnsBelongToPartners(pawn1, pawn2)) {
            throw new KeezException("SEVEN_SPLIT must play pawns of the same player or partners");
        }
        moves.add(new Move(pawn1, steps1, Direction.FORWARD));
        moves.add(new Move(pawn2, steps2, Direction.FORWARD));
    }

    public Play(Card card, Pawn pawn1, Pawn pawn2) {
        if (card.getCardType() != CardType.SWAP) {
            throw new KeezException("Only SWAP can swap two pawns");
        }
        if (pawnsBelongToSamePlayer(pawn1, pawn2) || pawnsBelongToPartners(pawn1, pawn2)) {
            throw new KeezException("Pawns must not belong to the same player nor to partners");
        }
        if (!pawnsInPlayArea(pawn1, pawn2)) {
            throw new KeezException("Only pawns in the PLAY area can be swapped");
        }
        swapPositions(pawn1, pawn2);
    }

    private void swapPositions(Pawn pawn1, Pawn pawn2) {
        int position1 = pawn1.getPosition();
        pawn1.setPosition(pawn2.getPosition());
        pawn2.setPosition(position1);
    }

    private boolean pawnsInPlayArea(Pawn pawn1, Pawn pawn2) {
        return pawn1.getArea() == Area.PLAY && pawn2.getArea() == Area.PLAY;
    }

    private boolean pawnsBelongToSamePlayer(Pawn pawn1, Pawn pawn2) {
        return pawn1.getPlayer() == pawn2.getPlayer();
    }

    private boolean pawnsBelongToPartners(Pawn pawn1, Pawn pawn2) {
        return pawn1.getPlayer() == pawn2.getPlayer().getPartner();
    }
}
