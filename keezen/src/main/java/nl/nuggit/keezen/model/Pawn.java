package nl.nuggit.keezen.model;

public class Pawn {

    private Player player;
    private Area area;
    private int position;
    private boolean hasMovedSinceStart;

    public Pawn(Player player) {
        this.player = player;
        this.area = Area.WAIT;
    }

    public Player getPlayer() {
        return player;
    }

    public Area getArea() {
        return area;
    }

    public void moveToPlayArea() {
        this.area = Area.PLAY;
        position = player.getStartPosition();
        hasMovedSinceStart = false;
    }

    public void moveToHomeArea() {
        this.area = Area.HOME;
        position = 0;
    }

    public void moveToWaitArea() {
        this.area = Area.WAIT;
        position = 0;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        hasMovedSinceStart = true;
    }

    public boolean collidesWith(Pawn other) {
        return other != this && area == other.getArea() && position == other.getPosition();
    }

    public boolean hasMovedSinceStart() {
        return hasMovedSinceStart;
    }
}

