package nl.nuggit.keezen.model.event;

import nl.nuggit.keezen.model.Pawn;

public abstract class MoveEvent {

    private Pawn actor;
    private int position;

    public MoveEvent(Pawn actor, int position) {
        this.actor = actor;
        this.position = position;
    }

    public Pawn getActor() {
        return actor;
    }

    public int getPosition() {
        return position;
    }
}
