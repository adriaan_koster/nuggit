package nl.nuggit.keezen.model;

public class Move {

    private final Pawn pawn;
    private final int steps;
    private final Direction direction;

    public Move(Pawn pawn, int steps, Direction direction) {
        this.pawn = pawn;
        this.steps = steps;
        this.direction = direction;
    }

    public Pawn getPawn() {
        return pawn;
    }

    public int getSteps() {
        return steps;
    }

    public Direction getDirection() {
        return direction;
    }
}
