package nl.nuggit.keezen.model.event;

import nl.nuggit.keezen.model.Pawn;

public class BlockedMoveEvent extends MoveEvent {

    private final String reason;

    public BlockedMoveEvent(Pawn actor, int position, String reason) {
        super(actor, position);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}
