package nl.nuggit.keezen.model.card;

public enum CardType {
    START_OR_ONE, START, TWELVE, SWAP, SEVEN_SPLIT, SIX, FIVE, FOUR_BACK, THREE, TWO;
}
