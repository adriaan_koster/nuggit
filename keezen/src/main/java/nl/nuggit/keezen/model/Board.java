package nl.nuggit.keezen.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import nl.nuggit.keezen.Settings;
import nl.nuggit.keezen.model.event.BlockedMoveEvent;
import nl.nuggit.keezen.model.event.JumpMoveEvent;
import nl.nuggit.keezen.model.event.LandMoveEvent;
import nl.nuggit.keezen.model.event.MoveEvent;

public class Board {

    private final List<Pawn> pawns;

    public Board(List<Pawn> pawns) {
        this.pawns = pawns;
    }

    public Stream<Pawn> getPawns() {
        return pawns.stream();
    }

    public Stream<MoveEvent> play(Move move) {
        validate(move);
        List<MoveEvent> moveEvents = new ArrayList<>();
        Area area = move.getPawn().getArea();
        int stepsLeft = move.getSteps();
        int position = move.getPawn().getPosition();
        while (stepsLeft > 0) {
            int newPosition = move.getDirection().step(position);
            if (area == Area.PLAY) {
                if (newPosition > move.getPawn().getPlayer().getEndPosition()) {
                    area = Area.HOME;
                    newPosition = 0;
                } else if (newPosition >= Settings.playPositions()) {
                    newPosition = 0;
                }
            } else {
                // area is HOME
                if (newPosition >= Settings.NUMBER_OF_PLAYERS) {
                    moveEvents.add(new BlockedMoveEvent(move.getPawn(), position, "Cannot move past end of home area"));
                    break;
                }
            }
            position = newPosition;
            stepsLeft--;
            handleCollisions(moveEvents, move.getPawn(), area, position);
        }
        moveEvents.add(new LandMoveEvent(move.getPawn(), position));
        return moveEvents.stream();
    }

    private void validate(Move move) {
        Pawn pawn = move.getPawn();
        Area area = pawn.getArea();
        if (area == Area.WAIT) {
            throw new KeezException("Cannot move a pawn which is in the waiting area");
        }
        if (!pawns.contains(pawn)) {
            throw new KeezException("Pawn does not exist on this board");
        }
    }

    private void handleCollisions(List<MoveEvent> moveEvents, Pawn pawn, final Area area, int position) {
        getPawns().filter(p -> p.collidesWith(pawn)).findAny().ifPresent(p -> {
            if (area == Area.PLAY) {
                if (p.hasMovedSinceStart()) {
                    moveEvents.add(new JumpMoveEvent(pawn, position, p));
                } else {
                    moveEvents.add(new BlockedMoveEvent(pawn, position, "Cannot jump over pawn in start position"));
                }
            } else if (area == Area.HOME) {
                moveEvents.add(new BlockedMoveEvent(pawn, position, "Cannot jump over pawns in home area"));
            }

        });
    }

}
