package nl.nuggit.design.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Bank {

    private List<LogItem> history = new ArrayList<>();

    public void execute(Transaction transaction, User user) {
        transaction.execute(user);
        log(transaction);
    }

    public void deposit(Account account, int amount) {
        account.deposit(amount);
        log(new SimpleLogItem(String.format("%s deposited on %s", amount, account)));
    }

    public void withdraw(Account account, int amount, User user) {
        account.withdraw(amount, user);
        log(new SimpleLogItem(String.format("%s withdrawn from %s", amount, account)));
    }

    public void log(LogItem logItem) {
        history.add(logItem);
    }

    public Stream<LogItem> getLogs() {
        return history.stream();
    }
}
