package nl.nuggit.design.model;

import java.time.LocalDateTime;

public interface LogItem {

    LocalDateTime time();

    String message();
}
