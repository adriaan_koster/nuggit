package nl.nuggit.design.model;

import java.time.LocalDateTime;

public class Transaction implements LogItem {

    private final Account from;
    private final Account to;
    private final long amount;

    private Status status = Status.NEW;
    private LocalDateTime completedAt;

    private enum Status {
        NEW,
        HISTORIC;
    }

    public Transaction(Account from, Account to, long amount) {
        if (from == to) {
            throw new IllegalArgumentException("Transaction accounts must be different");
        }
        if (amount < 0) {
            throw new IllegalArgumentException("Amount must be positive");
        }
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public void execute(User user) {
        if (status == Status.HISTORIC) {
            throw new IllegalArgumentException("Historic transaction cannot be executed again");
        }
        from.withdraw(amount, user);
        to.deposit(amount);
        status = Status.HISTORIC;
        completedAt = LocalDateTime.now();
    }

    public LocalDateTime getCompletedAt() {
        return completedAt;
    }

    @Override
    public LocalDateTime time() {
        return getCompletedAt();
    }

    @Override
    public String message() {
        return this.toString();
    }

    @Override
    public String toString() {
        return String.format("%s from %s to %s", amount, from, to);
    }
}
