package nl.nuggit.design.model;

public class Account {

    private final String iban;
    private final User owner;

    private long balance;

    public Account(String iban, User owner) {
        this.iban = iban;
        this.owner = owner;
    }

    public User getOwner() {
        return owner;
    }

    public long getBalance() {
        return balance;
    }

    public synchronized void withdraw(long amount, User user) {
        if (amount < 0) {
            throw new IllegalArgumentException("Cannot withdraw negative amount");
        }
        if (user != owner) {
            throw new IllegalArgumentException("Only the owner can withdraw from an account");
        }
        if (amount > balance) {
            throw new IllegalArgumentException("Cannot withdraw more than balance");
        }
        balance = balance - amount;
    }

    public synchronized void deposit(long amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Cannot deposit negative amount");
        }
        balance = balance + amount;
    }

    @Override
    public String toString() {
        return iban;
    }
}
