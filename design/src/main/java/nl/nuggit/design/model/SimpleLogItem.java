package nl.nuggit.design.model;

import java.time.LocalDateTime;

public class SimpleLogItem implements LogItem {

    private final String message;
    private LocalDateTime time = LocalDateTime.now();

    public SimpleLogItem(String message) {
        this.message = message;
    }

    @Override
    public LocalDateTime time() {
        return time;
    }

    @Override
    public String message() {
        return message;
    }
}

