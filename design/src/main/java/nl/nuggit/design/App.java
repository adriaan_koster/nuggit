package nl.nuggit.design;

import nl.nuggit.design.model.Account;
import nl.nuggit.design.model.Bank;
import nl.nuggit.design.model.Transaction;
import nl.nuggit.design.model.User;

public class App {

    public static void main(String[] args) {

        Bank bank = new Bank();

        User barry = new User("barry123", "Barry White");
        User adele = new User("addy4ever", "Adele");

        Account account1 = new Account("11111", barry);
        bank.deposit(account1, 871);

        Account account2 = new Account("22222", adele);
        bank.deposit(account2, 238);

        bank.execute(new Transaction(account1, account2, 412), barry);
        bank.execute(new Transaction(account2, account1, 43), adele);
        bank.execute(new Transaction(account1, account2, 166), barry);

        bank.withdraw(account2, 238, adele);

        bank.getLogs().forEach(x -> System.out.printf("%s: %s%n", x.time(), x.message()));

        System.out.printf("%s %s%n", account1, account1.getBalance());
        System.out.printf("%s %s%n", account2, account2.getBalance());
    }

}
