package nl.nuggit.sudoku;

import java.util.Iterator;

public class BoardPrinter {

    public static String print(Board board) {

        String[][] b = new String[31][31];

        for (int xy = 0; xy < 31; xy++) {
            b[xy][0] = "#";
            b[xy][10] = "#";
            b[xy][20] = "#";
            b[xy][30] = "#";
            b[0][xy] = "#";
            b[10][xy] = "#";
            b[20][xy] = "#";
            b[30][xy] = "#";
        }

        int ylines = 1;
        for (int y = 0; y < 9; y++) {

            if (y == 3) {
                ylines = 2;
            }
            if (y == 6) {
                ylines = 3;
            }

            int xlines = 1;
            for (int x = 0; x < 9; x++) {
                if (x == 3) {
                    xlines = 2;
                }
                if (x == 6) {
                    xlines = 3;
                }
                Square square = board.getSquare(x, y);
                if (square.getValue() > 0) {
                    if (!square.getCandidates().isEmpty()) {
                        System.err.println("candidates : " + square.getCandidates().size());
                    }
                    for (int cy = 0; cy < 3; cy++) {
                        for (int cx = 0; cx < 3; cx++) {
                            String cell = " ";
                            if (cy == 1 && cx == 1) {
                                cell = String.valueOf(square.getValue());
                            }
                            b[xlines + cx + (3 * x)][ylines + cy + (3 * y)] = cell;
                        }
                    }
                    b[xlines + 1 + (3 * x)][ylines + 1 + (3 * y)] = String.valueOf(square.getValue());
                }
                else {
                    Iterator<Integer> it = square.getCandidates().iterator();
                    for (int cy = 0; cy < 3; cy++) {
                        for (int cx = 0; cx < 3; cx++) {
                            String cell = ".";
                            if (it.hasNext()) {
                                cell = String.valueOf(it.next());
                            }
                            b[xlines + cx + (3 * x)][ylines + cy + (3 * y)] = cell;
                        }
                    }
                }
            }
        }

        StringBuilder builder = new StringBuilder();
        for (int y = 0; y < 31; y++) {
            for (int x = 0; x < 31; x++) {
                builder.append(b[x][y]);
            }
            builder.append("\n");
        }
        return builder.toString();
    }

}
