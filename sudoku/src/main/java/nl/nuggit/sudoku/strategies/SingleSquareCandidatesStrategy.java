package nl.nuggit.sudoku.strategies;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import nl.nuggit.sudoku.Area;
import nl.nuggit.sudoku.Board;
import nl.nuggit.sudoku.Strategy;
import nl.nuggit.sudoku.Square;

public class SingleSquareCandidatesStrategy implements Strategy {

    @Override
    public boolean applyTo(Board board) {
        System.out.println("SingleSquareCandidatesStrategy");
        boolean changesMade = false;
        for (Area area : board.getAreas()) {
            Map<Integer, Set<Square>> areaCandidateSquares = new HashMap<Integer, Set<Square>>();
            for (Square square : area.getSquares()) {
                for (Integer candidate : square.getCandidates()) {
                    Set<Square> squares = areaCandidateSquares.get(candidate);
                    if (squares == null) {
                        squares = new HashSet<Square>();
                        areaCandidateSquares.put(candidate, squares);
                    }
                    squares.add(square);
                }
            }
            boolean areaLogged = false;
            for (Entry<Integer, Set<Square>> entry : areaCandidateSquares.entrySet()) {
                if (entry.getValue().size() == 1) {
                    if (!areaLogged) {
                        System.out.println(area);
                        areaLogged = true;
                    }
                    entry.getValue().iterator().next().setValue(entry.getKey());
                    changesMade = true;
                }
            }
        }
        System.out.println("--");
        return changesMade;
    }
}
