package nl.nuggit.sudoku.strategies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import nl.nuggit.sudoku.Area;
import nl.nuggit.sudoku.Board;
import nl.nuggit.sudoku.Square;
import nl.nuggit.sudoku.Strategy;

public class PruneCandidatesByGroupsStrategy implements Strategy {

    @Override
    public boolean applyTo(Board board) {
        System.out.println("PruneCandidatesByGroupsStrategy");
        boolean changesMade = false;
        for (int groupSize = 2; groupSize < 9; groupSize++) {
            if (pruneCandidatesByGroups(board, groupSize)) {
                changesMade = true;
            }
            if (changesMade) {
                break;
            }
        }
        return changesMade;
    }

    private boolean pruneCandidatesByGroups(Board board, int groupSize) {
        System.out.println(String.format("pruneCandidatesByGroups of size %s", groupSize));
        boolean changesMade = false;
        for (Area area : board.getAreas()) {
            Set<Set<Integer>> candidateSets = collectCandidateSetsOfSize(area, groupSize);
            for (Set<Integer> candidateSet : candidateSets) {
                Set<Square> matches = findSquaresWithCandidateSubsetOf(area, candidateSet);
                if (matches.size() == groupSize) {
                    if (removeCandidatesFromOthers(area, candidateSet, matches)) {
                        changesMade = true;
                        System.out.println("Area " + area);
                        System.out.println("Matches " + Arrays.toString(matches.toArray()));
                        System.out.println("--");
                    }
                }
            }
        }
        return changesMade;
    }

    private Set<Set<Integer>> collectCandidateSetsOfSize(Area area, int groupSize) {
        Set<Integer> elements = new TreeSet<Integer>();
        for (Square square : area.getSquares()) {
            if (square.getValue() == 0) {
                elements.addAll(square.getCandidates());
            }
        }
        Set<Set<Integer>> result = new HashSet<Set<Integer>>();
        for (Set<Integer> set : powerSet(elements)) {
            if (set.size() == groupSize) {
                result.add(set);
            }
        }
        return result;
    }

    public Set<Set<Integer>> powerSet(Set<Integer> originalSet) {
        Set<Set<Integer>> sets = new HashSet<Set<Integer>>();
        if (originalSet.isEmpty()) {
            sets.add(new HashSet<Integer>());
            return sets;
        }
        List<Integer> list = new ArrayList<Integer>(originalSet);
        Integer head = list.get(0);
        Set<Integer> rest = new HashSet<Integer>(list.subList(1, list.size()));
        for (Set<Integer> set : powerSet(rest)) {
            Set<Integer> newSet = new HashSet<Integer>();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }
        return sets;
    }

    private Set<Square> findSquaresWithCandidateSubsetOf(Area area, Set<Integer> group) {
        Set<Square> matches = new HashSet<Square>();
        for (Square square : area.getSquares()) {
            if (square.getValue() == 0) {
                Set<Integer> candidates = square.getCandidates();
                if (group.containsAll(candidates) && candidates.size() >= 2) {
                    matches.add(square);
                }
            }
        }
        return matches;
    }

    private boolean removeCandidatesFromOthers(Area area, Set<Integer> candidatesToRemove, Set<Square> matches) {
        boolean changesMade = false;
        for (Square square : area.getSquares()) {
            if (square.getValue() == 0 && !matches.contains(square)) {
                Set<Integer> originalCandidates = new HashSet<Integer>(square.getCandidates());
                boolean setAltered = square.getCandidates().removeAll(candidatesToRemove);
                if (setAltered) {
                    System.out.println(String.format("(%s,%s) removed %s from %s", square.getX(), square.getY(), Arrays.toString(candidatesToRemove.toArray()), Arrays.toString(originalCandidates.toArray())));
                    changesMade = true;
                }
            }
        }
        return changesMade;
    }

}
