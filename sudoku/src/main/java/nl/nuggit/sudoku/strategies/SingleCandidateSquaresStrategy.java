package nl.nuggit.sudoku.strategies;

import nl.nuggit.sudoku.Board;
import nl.nuggit.sudoku.Strategy;
import nl.nuggit.sudoku.Square;

public class SingleCandidateSquaresStrategy implements Strategy {

    @Override
    public boolean applyTo(Board board) {
        System.out.println("SingleCandidateSquaresStrategy");
        boolean changesMade = false;
        for (Square square : board.getSquares()) {
            if (square.getCandidates().size() == 1) {
                int value = square.getCandidates().iterator().next();
                square.setValue(value);
                changesMade = true;
            }
        }
        System.out.println("--");
        return changesMade;
    }
}
