package nl.nuggit.sudoku.strategies;

import java.util.HashSet;
import java.util.Set;

import nl.nuggit.sudoku.Area;
import nl.nuggit.sudoku.Board;
import nl.nuggit.sudoku.Strategy;
import nl.nuggit.sudoku.Square;

public class ClaimByBlockStrategy implements Strategy {

    @Override
    public boolean applyTo(Board board) {
        System.out.println("ClaimByBlockStrategy");
        boolean changesMade = false;
        for (Area block : board.getBlocks()) {
            for (int value = 0; value < 9; value++) {
                Set<Square> squaresWithValue = getSquaresWithValue(block, value);
                Set<Area> rowsWithValue = new HashSet<Area>();
                Set<Area> columnsWithValue = new HashSet<Area>();
                for (Square squareWithValue : squaresWithValue) {
                    rowsWithValue.add(board.getRow(squareWithValue));
                    columnsWithValue.add(board.getColumn(squareWithValue));
                }
                boolean valuesRemoved = false;
                if (rowsWithValue.size() == 1) {
                    valuesRemoved = removeValueFromNonBlockSquares(value, rowsWithValue.iterator().next(), block);
                }
                if (columnsWithValue.size() == 1) {
                    valuesRemoved = removeValueFromNonBlockSquares(value, columnsWithValue.iterator().next(), block) || valuesRemoved;
                }
                if (valuesRemoved) {
                    System.out.println("Area " + block);
                    System.out.println("--");
                }
            }
        }
        return changesMade;
    }

    private Set<Square> getSquaresWithValue(Area block, int value) {
        Set<Square> blockSquaresWithCandidate = new HashSet<Square>();
        for (Square square : block.getSquares()) {
            if (square.getCandidates().contains(value)) {
                blockSquaresWithCandidate.add(square);
            }
        }
        return blockSquaresWithCandidate;
    }

    private boolean removeValueFromNonBlockSquares(int value, Area area, Area block) {
        boolean changesMade = false;
        System.out.println(String.format("Removing %s from squares in %s", value, area));
        for (Square square : area.getSquares()) {
            if (!block.getSquares().contains(square)) {
                if (square.getCandidates().remove(value)) {
                    changesMade = true;
                }
            }
        }
        return changesMade;
    }
}
