package nl.nuggit.sudoku;

/**
 * A strategy for solving a board
 */
public interface Strategy {

    /**
     * @param board a board
     * @return true if changes were made, false otherwise
     */
    boolean applyTo(Board board);
}
