package nl.nuggit.sudoku;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of a row, column or block within a board
 */
public class Area {

    public enum Shape {
        ROW, COLUMN, BLOCK;
    }

    private final Shape shape;
    private final List<Square> squares = new ArrayList<Square>();

    public Area(Shape shape) {
        this.shape = shape;
    }

    public Shape getShape() {
        return shape;
    }

    public void add(Square square) {
        squares.add(square);
    }

    public List<Square> getSquares() {
        return squares;
    }

    @Override
    public String toString() {
        return shape + (squares.size() > 0 ? (String.format(" containing (%s,%s)", squares.get(0).getX(), squares.get(0).getY())) : "");
    }
}
