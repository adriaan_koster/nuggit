package nl.nuggit.sudoku;

import java.util.Arrays;
import java.util.List;

import nl.nuggit.sudoku.strategies.ClaimByBlockStrategy;
import nl.nuggit.sudoku.strategies.PruneCandidatesByGroupsStrategy;
import nl.nuggit.sudoku.strategies.SingleCandidateSquaresStrategy;
import nl.nuggit.sudoku.strategies.SingleSquareCandidatesStrategy;

/**
 * Application which attempts to solve a Sudoku puzzle.
 */
public class SudokuSolver {

    private static final String puzzle1 = "080402060034000910960000084000216000000000000000357000840000075026000130090701040";
    private static final String puzzle2 = "080904500000000180560300947293100004000702000400003612839005061046000000002601030";
    private static final String puzzle3 = "000200063300005401001003980000000090000538000030000000026300500503700008470001000";

    public static void main(String[] args) {
        Board board = new Board(puzzle1);
        System.out.println(BoardPrinter.print(board));

        SudokuSolver solver = new SudokuSolver();
        solver.solve(board);

        System.out.println(BoardPrinter.print(board));
        if (!board.isSolved()) {
            System.out.println("No solution found");
        }
    }

    private List<Strategy> solvers = Arrays.asList(new Strategy[] {new SingleCandidateSquaresStrategy(), new SingleSquareCandidatesStrategy(), new PruneCandidatesByGroupsStrategy(), new ClaimByBlockStrategy()});

    public boolean solve(Board board) {
        boolean changesMade = true;
        while (!board.isSolved() && changesMade) {
            changesMade = false;
            for (Strategy solver : solvers) {
                if (solver.applyTo(board)) {
                    changesMade = true;
                    System.out.println(BoardPrinter.print(board));
                }
            }
        }
        return changesMade;
    }
}
