import java.util.*;

public class P {

	static char[] p;

	int i, x, y;

	P(int p, int q) {
		x = p;
		y = q;
		i = 12 * y + x;
	}

	P(int n) {
		i = n;
		x = i % 12;
		y = i / 12;
	}

	boolean p() {
		return p[i] == 'X';
	}

	List<P> n() {
		List<P> r = new Vector<>();
		for (int p = -1; p <= 1; p++) {
			for (int q = -1; q <= 1; q++) {
				P n = new P(x + q, y + p);
				if (n.x >= 0 && n.x < 12 && n.y >= 0 && n.y < 12 && n.p()) r.add(n);
			}
		}
		return r;
	}

	public static void main(String[] a) {
		p = a[0].toCharArray();
		List<P> s = null;
		P c = new P(0);
		while (c.i < 144) {
			List<P> t = new Vector<>();
			c.r(t, c);
			if (t.size() > 0 && (s == null || t.size() < s.size())) s = t;
			c = new P(c.i + 1);
		}
		System.out.print(s.size() + "=>" + s);
	}

	void r(List<P> c, P h) {
		if (h.p()) {
			c.add(h);
			p[h.i] = '-';
			for (P n : h.n()) r(c, n);
		}
	}

	public String toString() {
		return "(" + x + "," + y + ")";
	}
}
