package nl.nuggit.paint;

import java.util.ArrayList;
import java.util.List;

/**
 * SplashPaint is a main class which takes a sequence of 'X' and '0' characters as its only argument.
 * It interprets the characters as points forming a square painting and tries to find the smallest group of
 * neighboring 'X'-es. Neighbors are horizontal, vertical and diagonal. The first character of the input gets coordinate
 * (0,0), the second one (1,0) etc. The output is the size of the smallest group and its list of points.
 *
 * The class 'P' in the default package is a version of SplashPaint with minimized source code size.
 */
public class SplashPaint {

	public static final int SIZE = 12;
	static char[] painting;

	static class Point {
		int index;
		int x, y;

		Point(int painting, int q) {
			x = painting;
			y = q;
			index = SIZE * y + x;
		}

		Point(int n) {
			index = n;
			x = index % SIZE;
			y = index / SIZE;
		}

		boolean isValid() {
			return x >= 0 && x < SIZE && y >= 0 && y < SIZE;
		}

		boolean isPainted() {
			return isValid() && painting[index] == 'X';
		}

		List<Point> neighbors() {
			List<Point> result = new ArrayList<>();
			// walk over the 3x3 points around x,y
			for (int dy = -1; dy <= 1; dy++) {
				for (int dx = -1; dx <= 1; dx++) {
					Point neighbor = new Point(x + dx, y + dy);
					// the point itself is skipped because it is already removed from the painting
					if (neighbor.isPainted()) {
						result.add(neighbor);
					}
				}
			}
			return result;
		}

		public String toString() {
			return "(" + x + "," + y + ")";
		}
	}

	public static void main(String[] a) {
		painting = a[0].toCharArray();
		List<Point> smallestSplash = null;
		Point current = new Point(0);
		while (current.isValid()) {
			List<Point> splash = new ArrayList<>();
			findSplashRecursively(splash, current);
			if (splash.size() > 0 && (smallestSplash == null || splash.size() < smallestSplash.size())) {
				smallestSplash = splash;
			}
			current = new Point(current.index + 1);
		}
		System.out.println(smallestSplash.size() + "=>" + smallestSplash);
	}

	static void findSplashRecursively(List<Point> splash, Point head) {
		if (head.isPainted()) {
			splash.add(head);
			// remove visited points from the painting
			painting[head.index] = '-';
			for (Point n : head.neighbors()) {
				findSplashRecursively(splash, n);
			}
		}
	}
}
