package nl.nuggit.words.board;


public enum Booster implements Stringifiable {
	DOUBLE_LETTER("=", 2, 1),

	TRIPLE_LETTER("%", 3, 1),

	DOUBLE_WORD("2", 1, 2),

	TRIPLE_WORD("3", 1, 3);

	private String display;
	private int letterFactor;
	private int wordFactor;

	private Booster(String display, int letterFactor, int wordFactor) {
		this.display = display;
		this.letterFactor = letterFactor;
		this.wordFactor = wordFactor;
	}

	public int letterFactor() {
		return letterFactor;
	}

	public int wordFactor() {
		return wordFactor;
	}

	@Override
	public String toString() {
		return stringify();
	}

	public String stringify() {
		return display;
	}
	
	public static Booster deStringify(String value) {
		for (Booster booster : values()) {
			if (booster.display.equals(value)) {
				return booster;
			}
		}
		return null;
	}
}
