package nl.nuggit.words.board;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import nl.nuggit.words.language.Language;
import nl.nuggit.words.language.Letter;

public class BagOfTiles implements Stringifiable {

	private final List<Tile> tiles = new ArrayList<Tile>();
	private static final Random RANDOM = new Random();

	public BagOfTiles(Language language) {
		for (Letter letter : language.getLetters()) {
			for (int i = 0; i < letter.getCount(); i++) {
				tiles.add(new Tile(letter));
			}
		}
		Collections.sort(tiles, Tile.COMPARATOR);
	}

	public Tile drawTile() {
		int item = RANDOM.nextInt(tiles.size());
		Iterator<Tile> it = tiles.iterator();
		while (it.hasNext()) {
			Tile tile = it.next();
			if (item <= 0) {
				it.remove();
				return tile;
			}
			item--;
		}
		throw new RuntimeException("Failed to draw tile");
	}

	public void fillTray(Tray tray) {
		while (!tray.isFull() && !isEmpty()) {
			tray.addTile(drawTile());
		}
	}

	public Tile remove(Character symbol) {
		Iterator<Tile> it = tiles.iterator();
		while (it.hasNext()) {
			Tile tile = it.next();
			if (tile.getLetter().getSymbol().equals(symbol)) {
				it.remove();
				return tile;
			}
		}
		return null;
	}

	public Set<Tile> remove(Collection<Tile> tilesToRemove) {
		Set<Tile> removedTiles = new HashSet<Tile>();
		for (Tile tile : tilesToRemove) {
			Letter letter = tile.getLetter();
			Character symbol = letter.getSymbol();
			if (letter instanceof AppliedBlank) {
				symbol = Language.BLANK;
			}
			Tile removedTile = remove(symbol);
			if (removedTile != null) {
				removedTiles.add(tile);
			}
		}
		return removedTiles;
	}

	public List<Tile> getTiles() {
		return Collections.unmodifiableList(tiles);
	}

	public int size() {
		return tiles.size();
	}

	public int vowelCount() {
		int result = 0;
		for (Tile tile : tiles) {
			if (tile.getLetter().isVowel()) {
				result++;
			}
		}
		return result;
	}

	public int totalValue() {
		int result = 0;
		for (Tile tile : tiles) {
			result += tile.getLetter().getValue();
		}
		return result;
	}

	public boolean isEmpty() {
		return tiles.size() == 0;
	}

	@Override
	public String toString() {
		return String.format("%s (tiles:%s vowels:%s, total_value:%s)",
				stringify(), size(), vowelCount(), totalValue());
	}

	public String stringify() {
		StringBuilder sb = new StringBuilder();
		for (Tile tile : tiles) {
			sb.append(tile.getLetter().getSymbol());
		}
		return sb.toString();
	}

	public static BagOfTiles fromString(String letters, Language language) {
		BagOfTiles bag = new BagOfTiles(language);
		for (int i = 0; i < letters.length(); i++) {
			bag.tiles.add(new Tile(language.getLetter(letters.charAt(i))));
		}
		return bag;
	}

}
