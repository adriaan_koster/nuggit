package nl.nuggit.words.board;

public class Line {

	public enum Direction {
		COLUMN, ROW;
	}
	
	private final Direction direction;
	private final int position;
	private final Square[] squares;

	public Line(Direction direction, int position, int size) {
		this.direction = direction;
		this.position = position;
		this.squares = new Square[size];
	}

	public Direction getDirection() {
		return direction;
	}

	public int getPosition() {
		return position;
	}

	public Square[] getSquares() {
		return squares;
	}

	public int size() {
		return squares.length;
	}

	@Override
	public String toString() {
		return direction + " " + position;
	}
}
