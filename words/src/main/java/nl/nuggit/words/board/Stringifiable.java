package nl.nuggit.words.board;

public interface Stringifiable {
	String stringify();
}
