package nl.nuggit.words.board;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nl.nuggit.words.board.Line.Direction;
import nl.nuggit.words.language.Language;
import nl.nuggit.words.language.Letter;

public class Board implements Stringifiable {

	private final static String LF = System.getProperty("line.separator");
	private String name = "Board" + System.currentTimeMillis();;
	private final int size;
	private final Square[][] squares;
	private final Line[] columns;
	private final Line[] rows;

	public Board(int size) {
		this.size = size;
		squares = new Square[size][size];
		for (int column = 0; column < size; column++) {
			for (int row = 0; row < size; row++) {
				squares[column][row] = new Square();
			}
		}
		int mid = size / 2;
		squares[mid][mid].setCenterSquare(true);

		columns = new Line[size];
		for (int column = 0; column < size; column++) {
			columns[column] = new Line(Direction.COLUMN, column, size);
			for (int row = 0; row < size; row++) {
				columns[column].getSquares()[row] = squares[column][row];
			}
		}

		rows = new Line[size];
		for (int row = 0; row < size; row++) {
			rows[row] = new Line(Direction.ROW, row, size);
			for (int column = 0; column < size; column++) {
				rows[row].getSquares()[column] = squares[column][row];
			}
		}
	}

	public Board(String boardState, Language language) {
		this(determineSize(boardState));
		int index = 0;
		for (int row = 0; row < size; row++) {
			for (int column = 0; column < size; column++) {
				String squareState = boardState.substring(index, index + 1);
				while (LF.indexOf(squareState) >= 0
						&& index < boardState.length()) {
					index++;
					squareState = boardState.substring(index, index + 1);
				}
				Square square = getSquare(column, row);
				square.setBooster(Booster.deStringify(squareState));
				square.setTile(Tile.destringify(squareState, language));
				index++;
			}
		}
	}

	private static int determineSize(String boardState) {
		int sizeGuess = boardState.indexOf(LF);
		if (sizeGuess < 1) {
			throw new IllegalArgumentException("Cannot determine board size"
					+ boardState);
		}
		return sizeGuess;
	}

	public int size() {
		return size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Square getSquare(int column, int row) {
		return squares[column][row];
	}

	public Set<Tile> getTiles() {
		Set<Tile> tiles = new HashSet<Tile>();
		for (int column = 0; column < size; column++) {
			for (int row = 0; row < size; row++) {
				Tile tile = squares[column][row].getTile();
				if (tile != null) {
					tiles.add(tile);
				}
			}
		}
		return tiles;
	}

	public List<Line> getLines() {
		List<Line> lines = new ArrayList<Line>();
		for (int column = 0; column < size; column++) {
			lines.add(columns[column]);
		}
		for (int row = 0; row < size; row++) {
			lines.add(rows[row]);
		}
		return lines;
	}

	public void addWord(String wordStr, Line.Direction direction, int position,
			int startPosition, Language language) {
		// Line line;
		int col, row;
		if (direction == Direction.COLUMN) {
			// line = columns[position];
			row = startPosition;
			col = position;
		} else {
			// line = rows[position];
			row = position;
			col = startPosition;
		}
		// Word word = new Word(line, startPosition);
		for (Character c : wordStr.toCharArray()) {
			Square square = getSquare(col, row);
			if (square.getTile() == null) {
				Letter letter = language.getLetter(c);
				if(Character.isUpperCase(c)) {
					letter = new AppliedBlank(letter);
				}
				square.setTile(new Tile(letter));
			}
			if (direction == Direction.COLUMN) {
				row++;
			} else {
				col++;
			}
		}
	}

	public Line getOrthogonalLine(Line line, int position) {
		Line orthogonal = null;
		if (line.getDirection() == Direction.ROW) {
			orthogonal = columns[position];
		} else if (line.getDirection() == Direction.COLUMN) {
			orthogonal = rows[position];
		} else {
			throw new IllegalStateException("Unrecognized direction : "
					+ line.getDirection());
		}
		return orthogonal;
	}

	public List<Character> getLetters() {
		List<Character> letters = new ArrayList<Character>();
		for (int column = 0; column < size; column++) {
			for (int row = 0; row < size; row++) {
				Tile tile = squares[column][row].getTile();
				if (tile != null) {
					letters.add(tile.getLetter().getSymbol());
				}
			}
		}
		return letters;
	}

	public List<List<Character>> getSymbolsPerLine() {
		List<List<Character>> symbolsPerLine = new ArrayList<List<Character>>();
		for (int column = 0; column < size; column++) {
			List<Character> columnSymbols = new ArrayList<Character>();
			for (int row = 0; row < size; row++) {
				addSymbolToLine(column, row, columnSymbols);
			}
			symbolsPerLine.add(columnSymbols);
		}
		for (int row = 0; row < size; row++) {
			List<Character> rowSymbols = new ArrayList<Character>();
			for (int column = 0; column < size; column++) {
				addSymbolToLine(column, row, rowSymbols);
			}
			symbolsPerLine.add(rowSymbols);
		}
		return symbolsPerLine;
	}

	private void addSymbolToLine(int column, int row,
			List<Character> lineSymbols) {
		Tile tile = squares[column][row].getTile();
		if (tile != null) {
			lineSymbols.add(tile.getLetter().getSymbol());
		}
	}

	@Override
	public String toString() {
		return name + LF + stringify();
	}

	public String stringify() {
		StringBuilder sb = new StringBuilder();
		for (int row = 0; row < size; row++) {
			for (int column = 0; column < size; column++) {
				sb.append(squares[column][row].stringify());
			}
			sb.append(LF);
		}
		return sb.toString();
	}
}