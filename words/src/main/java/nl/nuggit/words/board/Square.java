package nl.nuggit.words.board;


public class Square implements Stringifiable {

	private Tile tile;
	private Booster booster;
	private boolean centerSquare;

	public void setCenterSquare(boolean centerSquare) {
		this.centerSquare = centerSquare;
	}

	public boolean isCenterSquare() {
		return centerSquare;
	}

	public Tile getTile() {
		return tile;
	}

	public void setTile(Tile tile) {
		if (this.tile != null) {
			throw new IllegalStateException("Square is already occupied");
		}
		this.tile = tile;
	}

	public Booster getBooster() {
		return booster;
	}

	public void setBooster(Booster booster) {
		this.booster = booster;
	}

	@Override
	public String toString() {
		return stringify();
	}

	public String stringify() {
		String display;
		if (tile != null) {
			display = tile.stringify();
		} else if (booster != null) {
			display = booster.stringify();
		} else {
			display = ".";
		}
		return display;
	}
}
