package nl.nuggit.words.board;

import nl.nuggit.words.language.Letter;

public class AppliedBlank implements Letter {

	private final Letter letter;

	public AppliedBlank(Letter letter) {
		this.letter = letter;
	}

	public Character getSymbol() {
		return letter.getSymbol();
	}

	public int getCount() {
		return 0;
	}

	public int getValue() {
		return 0;
	}

	public boolean isVowel() {
		return letter.isVowel();
	}
}
