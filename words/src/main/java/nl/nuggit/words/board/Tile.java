package nl.nuggit.words.board;

import java.util.Comparator;

import nl.nuggit.words.language.Language;
import nl.nuggit.words.language.Letter;

/**
 * A tile with a certain letter on it. There can be multiple tiles with the same
 * letter.
 * 
 * @author Adriaan
 */
public class Tile implements Stringifiable {

	private final Letter letter;

	public Tile(Letter letter) {
		this.letter = letter;
	}

	public Letter getLetter() {
		return letter;
	}

	@Override
	public String toString() {
		return stringify();
	}

	public String stringify() {
		String value = String.valueOf(letter.getSymbol());
		if (letter instanceof AppliedBlank) {
			value = value.toLowerCase();
		}
		return value;
	}

	public static Tile destringify(String tileState, Language language) {
		if (tileState == null || tileState.length() != 1) {
			throw new IllegalArgumentException(
					"Tile state should have length 1");
		}
		Tile tile = null;
		Character symbol = tileState.charAt(0);
		Letter letter = language.getLetter(symbol);
		if (letter != null) {
			if (Character.isLowerCase(symbol)) {
				letter = new AppliedBlank(letter);
			}
			tile = new Tile(letter);
		}
		return tile;
	}

	/**
	 * Sorts tiles alphabetically
	 */
	public static final Comparator<Tile> COMPARATOR = new Comparator<Tile>() {
		public int compare(Tile tile1, Tile tile2) {
			if (tile1 == null && tile2 == null) {
				return 0;
			} else if (tile1 == null) {
				return 1;
			} else if (tile2 == null) {
				return -1;
			}
			return tile1.getLetter().getSymbol()
					- tile2.getLetter().getSymbol();
		}
	};
}
