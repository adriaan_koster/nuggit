package nl.nuggit.words.board;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import nl.nuggit.words.language.Language;
import nl.nuggit.words.language.Letter;

public class Tray implements Stringifiable {

	private final int size;
	private final List<Tile> tiles;

	public Tray(int size) {
		this.size = size;
		tiles = new ArrayList<Tile>();
	}

	public Tray(String letters, int size, Language language) {
		this(size);
		for (int i = 0; i < letters.length(); i++) {
			addTile(new Tile(language.getLetter(letters.charAt(i))));
		}
	}

	public void addTile(Tile tile) {
		if (tiles.size() < size) {
			tiles.add(tile);
		} else {
			throw new IllegalStateException("Tray is full");
		}
	}

	public void addTiles(Collection<Tile> newTiles) {
		for (Tile tile : newTiles) {
			addTile(tile);
		}
	}

	public void removeTiles(Collection<Tile> tilesToRemove) {
		for (Tile tileToRemove : tilesToRemove) {
			Letter letter = tileToRemove.getLetter();
			Character symbolToRemove;
			if (letter instanceof AppliedBlank) {
				symbolToRemove = Language.BLANK;
			} else {
				symbolToRemove = letter.getSymbol();
			}
			Iterator<Tile> it = tiles.iterator();
			while (it.hasNext()) {
				Tile tile = it.next();
				if (tile.getLetter().getSymbol() == symbolToRemove) {
					it.remove();
					break;
				}
			}
		}
	}

	public List<Character> getSymbols() {
		List<Character> symbols = new ArrayList<Character>();
		for (Tile tile : tiles) {
			symbols.add(tile.getLetter().getSymbol());
		}
		return symbols;
	}

	public List<Tile> getTiles() {
		return new ArrayList<Tile>(tiles);
	}

	public int size() {
		return size;
	}

	public int getTileCount() {
		return tiles.size();
	}

	public boolean isFull() {
		return tiles.size() >= size;
	}

	@Override
	public String toString() {
		return stringify();
	}

	public String stringify() {
		StringBuilder sb = new StringBuilder();
		for (Tile tile : tiles) {
			sb.append(tile.getLetter().getSymbol());
		}
		return sb.toString();
	}
}
