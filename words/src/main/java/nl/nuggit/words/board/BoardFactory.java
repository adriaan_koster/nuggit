package nl.nuggit.words.board;


public class BoardFactory {

	public static Board createStandardBoard() {
		Board board = new Board(15);
		board.getSquare(4, 0).setBooster(Booster.TRIPLE_WORD);
		board.getSquare(10, 0).setBooster(Booster.TRIPLE_WORD);
		board.getSquare(0, 4).setBooster(Booster.TRIPLE_WORD);
		board.getSquare(14, 4).setBooster(Booster.TRIPLE_WORD);
		board.getSquare(0, 10).setBooster(Booster.TRIPLE_WORD);
		board.getSquare(14, 10).setBooster(Booster.TRIPLE_WORD);
		board.getSquare(4, 14).setBooster(Booster.TRIPLE_WORD);
		board.getSquare(10, 14).setBooster(Booster.TRIPLE_WORD);

		board.getSquare(2, 2).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(12, 2).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(7, 3).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(4, 4).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(10, 4).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(3, 7).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(11, 7).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(4, 10).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(10, 10).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(7, 11).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(2, 12).setBooster(Booster.DOUBLE_WORD);
		board.getSquare(12, 12).setBooster(Booster.DOUBLE_WORD);

		board.getSquare(0, 0).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(14, 0).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(5, 1).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(9, 1).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(3, 3).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(11, 3).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(1, 5).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(5, 5).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(9, 5).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(13, 5).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(1, 9).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(5, 9).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(9, 9).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(9, 13).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(3, 11).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(11, 11).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(5, 13).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(9, 13).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(0, 14).setBooster(Booster.TRIPLE_LETTER);
		board.getSquare(14, 14).setBooster(Booster.TRIPLE_LETTER);

		board.getSquare(7, 0).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(1, 1).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(13, 1).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(6, 2).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(8, 2).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(6, 4).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(8, 4).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(2, 6).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(4, 6).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(10, 6).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(12, 6).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(0, 7).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(14, 7).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(2, 8).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(4, 8).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(10, 8).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(12, 8).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(6, 10).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(8, 10).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(6, 12).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(8, 12).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(1, 13).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(13, 13).setBooster(Booster.DOUBLE_LETTER);
		board.getSquare(7, 14).setBooster(Booster.DOUBLE_LETTER);

		return board;
	}
}
