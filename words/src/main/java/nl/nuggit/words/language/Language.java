package nl.nuggit.words.language;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Language {

	public static final Character BLANK = '_';
	private static final int MAX_WORD_LENGTH = 15;

	private final Set<String> words;
	private final Letter[] letters;
	private final String name;

	private Language(String[] dictionaries, Letter[] letters, String name) {
		System.out.println("Reading dictionaries: "
				+ Arrays.toString(dictionaries));
		this.words = readDictionaries(dictionaries);
		System.out.println(words.size() + " words found");
		this.letters = letters;
		this.name = name;
	}

	/**
	 * @return the letters in this language
	 */
	public Letter[] getLetters() {
		return letters;
	}

	/**
	 * @param symbol
	 *            the symbol to get the Letter for
	 * @return the matching Letter or null if not found
	 */
	public Letter getLetter(Character symbol) {
		Character ucSymbol = Character.toUpperCase(symbol);
		for (Letter letter : letters) {
			if (letter.getSymbol().equals(ucSymbol)) {
				return letter;
			}
		}
		return null;
	}

	/**
	 * @return gets the words in this language
	 */
	public Set<String> getWords() {
		return Collections.unmodifiableSet(words);
	}

	/**
	 * Determines if a word exists in this language
	 * 
	 * @param word
	 *            the word to be found
	 * @return true if the word is found, false otherwise
	 */
	public boolean hasWord(String word) {
		return words.contains(word);
	}

	private Set<String> readDictionaries(String[] dictionaries) {
		Set<String> words = new HashSet<String>();
		for (String dictionary : dictionaries) {
			InputStream is = ClassLoader.class.getResourceAsStream("/"
					+ dictionary);
			if (is == null) {
				System.out.println("Could not read dictionary: " + dictionary);
				continue;
			}
			Scanner scanner = new Scanner(is);
			while (scanner.hasNextLine()) {
				String word = scanner.nextLine().trim();
				if (word != null && word.length() > 1
						&& word.length() <= MAX_WORD_LENGTH && alphaOnly(word)) {
					words.add(word.toUpperCase());
				}
			}
			scanner.close();
		}
		return words;
	}

	private boolean alphaOnly(String word) {
		for (int i = 0; i < word.length(); ++i) {
			if (!Character.isLetter(word.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	private static class Dutch {
		private static final Language INSTANCE = new Language(new String[] {
				"OpenTaal-210G-basis-gekeurd.txt",
				"OpenTaal-210G-flexievormen.txt" }, NL.values(), "Nederlands");
	}

	private static class DrawSome {
		private static final Language INSTANCE = new Language(
				new String[] { "wordlist.csv", }, NL.values(), "DrawSome-NL");
	}

	public static Language dutch() {
		return Dutch.INSTANCE;
	}

	public static Language drawsome() {
		return DrawSome.INSTANCE;
	}

	@Override
	public String toString() {
		return name;
	}
}
