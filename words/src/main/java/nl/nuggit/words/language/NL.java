package nl.nuggit.words.language;

public enum NL implements Letter {

	A(7, 1, true),

	B(2, 4, false),

	C(2, 5, false),

	D(5, 2, false),

	E(18, 1, true),

	F(2, 4, false),

	G(3, 3, false),

	H(2, 4, false),

	I(4, 2, true),

	J(2, 4, false),

	K(3, 3, false),

	L(3, 3, false),

	M(3, 3, false),

	N(11, 1, false),

	O(6, 1, true),

	P(2, 4, false),

	Q(1, 10, false),

	R(5, 2, false),

	S(5, 2, false),

	T(5, 2, false),

	U(3, 2, true),

	V(2, 4, false),

	W(2, 5, false),

	X(1, 8, false),

	Y(1, 8, false),

	Z(2, 5, false),

	_(2, 0, false);

	private int value;
	private int count;
	private boolean vowel;

	private NL(int count, int value, boolean vowel) {
		this.value = value;
		this.count = count;
		this.vowel = vowel;
	}

	public int getCount() {
		return count;
	}

	public Character getSymbol() {
		return name().charAt(0);
	}

	public int getValue() {
		return value;
	}

	public boolean isVowel() {
		return vowel;
	}
}
