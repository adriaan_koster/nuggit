package nl.nuggit.words.language;

/**
 * A unique letter
 * 
 * @author Adriaan
 */
public interface Letter {

	/**
	 * @return the letter symbol
	 */
	Character getSymbol();

	/**
	 * @return the number of occurrences
	 */
	int getCount();

	/**
	 * @return the game value
	 */
	int getValue();
	
	
	/**
	 * @return true if this is a vowel, false otherwise
	 */
	boolean isVowel();
}