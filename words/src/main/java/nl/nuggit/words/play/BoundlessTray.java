package nl.nuggit.words.play;

import nl.nuggit.words.board.Tray;

/**
 * A Tray which can contain an unlimited amount of Tiles. The tray size is
 * maintained to allow drawing a limited number of tiles in one turn. This is
 * used for finding possible words for the opponent.
 * 
 * @author Adriaan
 */
public class BoundlessTray extends Tray {

	private int size;

	public BoundlessTray(int size) {
		super(Integer.MAX_VALUE);
		this.size = size;
	}

	public int size() {
		return size;
	}
}
