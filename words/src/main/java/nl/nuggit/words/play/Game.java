package nl.nuggit.words.play;

import java.util.ArrayList;
import java.util.List;

import nl.nuggit.words.board.BagOfTiles;
import nl.nuggit.words.board.Board;
import nl.nuggit.words.board.BoardFactory;
import nl.nuggit.words.board.Tray;
import nl.nuggit.words.language.Language;

public class Game {

	private static final int MAX_COUNTER_CHECKS = 3;
	private static final int TRAY_SIZE = 7;

	private Language language;
	private final Board board;
	private final BagOfTiles bag;
	private final List<Player> players;

	public Game() {
		language = Language.dutch();
		board = BoardFactory.createStandardBoard();
		bag = new BagOfTiles(language);
		players = new ArrayList<Player>();
	}

	public void play() {

		boolean moveMade;
		do {
			System.out.println(board.toString());
			moveMade = false;
			for (Player player : players) {
				if (player.isCpuControlled()) {
					moveMade = playBestWordFor(player) || moveMade;
				} else {
					// TODO: let human player make move
				}
			}
		} while (moveMade);

		System.out.println("Game ended");
	}

	private boolean playBestWordFor(Player player) {
		List<Word> words = player.suggestWords();
		if (words.size() == 0) {
			return false;
		}
		int bestWordIndex = 0;
		if (false && words.size() > 1) {
			int bestAdjustedScore = Integer.MIN_VALUE;
			for (int i = 0; i < Math.max(MAX_COUNTER_CHECKS, words.size()); i++) {
				Word word = words.get(i);
				int adjustedScore = word.getScore() - bestCounterScore(player);
				if (adjustedScore > bestAdjustedScore) {
					bestAdjustedScore = adjustedScore;
					bestWordIndex = i;
				}
			}
		}
		player.play(words.get(bestWordIndex));
		bag.fillTray(player.getTray());
		System.out.println(player);
		return true;
	}

	private int bestCounterScore(Player player) {
		Board virtualBoard = new Board(board.stringify(), language);
		virtualBoard.setName("virtualboard");
		System.out.println(virtualBoard);
		BagOfTiles virtualBag = new BagOfTiles(language);
		virtualBag.remove(board.getTiles());
		virtualBag.remove(player.getTray().getTiles());
		BoundlessTray virtualTray = new BoundlessTray(7);
		virtualTray.addTiles(virtualBag.getTiles());
		Player virtualOpponent = new Player("virtual", virtualBoard,
				virtualTray, language, true);
		List<Word> vwords = virtualOpponent.suggestWords();
		int bestCounterScore = 0;
		if (vwords.size() > 0) {
			bestCounterScore = vwords.get(0).getScore();
		}
		return bestCounterScore;
	}

	public void addPlayer(String name, boolean cpuControlled) {
		Tray tray = new Tray(TRAY_SIZE);
		bag.fillTray(tray);
		players.add(new Player(name, board, tray, language, cpuControlled));
	}

	public Language getLanguage() {
		return language;
	}

	public Board getBoard() {
		return board;
	}

	public BagOfTiles getBag() {
		return bag;
	}

	public List<Player> getPlayers() {
		return players;
	}
}
