package nl.nuggit.words.play;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import nl.nuggit.words.board.AppliedBlank;
import nl.nuggit.words.board.Board;
import nl.nuggit.words.board.Booster;
import nl.nuggit.words.board.Line;
import nl.nuggit.words.board.Square;
import nl.nuggit.words.board.Tile;
import nl.nuggit.words.board.Tray;
import nl.nuggit.words.language.Language;
import nl.nuggit.words.language.Letter;

public class Player {

	private final Language language;
	private final Board board;
	private final Tray tray;
	private int score;
	private String name;
	private boolean cpuControlled;

	/**
	 * Creates a CPU controlled player
	 */
	public Player(String name, Board board, Tray tray, Language language) {
		this(name, board, tray, language, true);
	}

	/**
	 * Creates a player
	 */
	public Player(String name, Board board, Tray tray, Language language,
			boolean cpuControlled) {
		this.name = name;
		this.language = language;
		this.board = board;
		this.tray = tray;
		this.cpuControlled = cpuControlled;
	}

	public List<Word> suggestWords() {
		System.out.println(String.format("%s finding possible words with %s ",
				name, tray));
		Set<String> candidates = findCandidates();
		System.out.println(candidates.size() + " candidates found: "
				+ candidates);
		List<Word> words = findWords(candidates);
		System.out.println(words.size() + " words: " + words);
		return words;
	}

	private Set<String> findCandidates() {
		Set<String> candidates = new HashSet<String>(language.getWords());
		List<Character> traySymbols = tray.getSymbols();
		Iterator<String> it = candidates.iterator();
		while (it.hasNext()) {
			String word = it.next();
			List<List<Character>> boardLines = board.getSymbolsPerLine();
			find_possible_line: {
				for (List<Character> lineSymbols : boardLines) {
					if (isWordPossible(traySymbols, lineSymbols, word)) {
						break find_possible_line;
					}
				}
				it.remove();
			}
		}
		return candidates;
	}

	private boolean isWordPossible(List<Character> traySymbols,
			List<Character> lineSymbols, String word) {
		List<Character> trayCopy = new ArrayList<Character>(traySymbols);
		List<Character> boardCopy = new ArrayList<Character>(lineSymbols);
		boolean allAvailable = true;
		boolean atLeastOneOnTray = false;
		for (int i = 0; i < word.length(); i++) {
			Character character = word.charAt(i);
			if (trayCopy.remove(character)) {
				atLeastOneOnTray = true;
			} else if (!boardCopy.remove(character)) {
				if (!trayCopy.remove(Language.BLANK)) {
					allAvailable = false;
					break;
				}
			}
		}
		return allAvailable && atLeastOneOnTray;
	}

	private List<Word> findWords(Set<String> candidates) {
		List<Word> words = new ArrayList<Word>();
		boolean boardIsEmpty = board.getLetters().size() == 0;
		int middlePosition = board.size() / 2;
		List<Character> traySymbols = tray.getSymbols();
		for (String candidate : candidates) {
			for (Line line : board.getLines()) {
				if (boardIsEmpty && line.getPosition() != middlePosition) {
					continue;
				}
				int maxStartPosition = line.size() - candidate.length();
				for (int startPosition = 0; startPosition <= maxStartPosition; startPosition++) {
					if (startPosition > 0
							&& line.getSquares()[startPosition - 1].getTile() != null) {
						// there is a tile directly before the start
						continue;
					}
					if (boardIsEmpty && startPosition > middlePosition) {
						continue;
					}
					int lastPosition = startPosition + candidate.length() - 1;
					if (lastPosition < (line.size() - 1)
							&& line.getSquares()[lastPosition + 1].getTile() != null) {
						// there is a tile directly after the end
						continue;
					}
					if (boardIsEmpty && lastPosition < middlePosition) {
						continue;
					}
					Word word = tryWordOnPosition(candidate, line,
							startPosition, traySymbols, boardIsEmpty);
					if (word != null) {
						words.add(word);
					}
				}
			}
		}
		Collections.sort(words, Word.COMPARATOR);
		return words;
	}

	private Word tryWordOnPosition(String candidate, Line line, int position,
			List<Character> traySymbols, boolean boardIsEmpty) {
		// if (candidate.equals("MEDUSA")) {
		// if (line.getDirection() == Direction.ROW
		// && line.getPosition() == 10 && position == 9) {
		// System.out.println(String.format(
		// "Candidate %s on %s position %s", candidate, line,
		// position));
		// }
		// }
		List<Character> trayCopy = new ArrayList<Character>(traySymbols);
		boolean fullMatch = true;
		int tilesTakenFromTray = 0;
		boolean boardTileUsed = false;
		Word word = new Word(line, position);
		int score = 0;
		int wordFactor = 1;
		for (int wordIndex = 0; wordIndex < candidate.length(); wordIndex++) {
			Character symbol = candidate.charAt(wordIndex);
			Square square = line.getSquares()[position + wordIndex];
			Tile tile = square.getTile();
			int letterValue;
			if (tile != null) {
				if (tile.getLetter().getSymbol().equals(symbol)) {
					// matching tile on the board
					boardTileUsed = true;
					word.getTiles().add(tile);
					letterValue = tile.getLetter().getValue();
				} else {
					// tile mismatch on board
					fullMatch = false;
					break;
				}
			} else {
				Letter letter = language.getLetter(symbol);
				if (trayCopy.remove(symbol)) {
					// we place a matching tile
					tilesTakenFromTray++;
					word.getTiles().add(new Tile(letter));
					Booster booster = square.getBooster();
					letterValue = letter.getValue();
					if (booster != null) {
						letterValue = letterValue * booster.letterFactor();
						wordFactor = wordFactor * booster.wordFactor();
					}
				} else if (trayCopy.remove(Language.BLANK)) {
					// we place a blank tile
					tilesTakenFromTray++;
					word.getTiles().add(new Tile(new AppliedBlank(letter)));
					letterValue = 0;

				} else {
					// we don't have a suitable tile
					fullMatch = false;
					break;
				}
				if (tilesTakenFromTray > tray.size()) {
					break;
				}
				// we placed a tile so check for crosswords
				Boolean validCrossword = checkCrossword(line, position, word,
						wordIndex);
				if (validCrossword != null) {
					if (validCrossword) {
						boardTileUsed = true;
					} else {
						fullMatch = false;
						break;
					}
				}
			}
			score += letterValue;
		}
		if (!fullMatch || tilesTakenFromTray == 0
				|| tilesTakenFromTray > tray.size()
				|| !(boardTileUsed || boardIsEmpty)) {
			return null;
		}
		score = score * wordFactor;
		word.addScore(score);
		if (tilesTakenFromTray == 7) {
			word.addScore(40);
		}
		return word;
	}

	private Boolean checkCrossword(Line line, int position, Word word,
			int wordIndex) {
		Line crossingLine = board.getOrthogonalLine(line, position + wordIndex);
		if (!hasAdjacentTiles(line, crossingLine)) {
			return null;
		}
		// find start
		int startPosition = line.getPosition();
		while (startPosition > 0
				&& crossingLine.getSquares()[startPosition - 1].getTile() != null) {
			startPosition--;
		}
		// find end
		int endPosition = line.getPosition();
		while (endPosition < (crossingLine.getSquares().length - 1)
				&& crossingLine.getSquares()[endPosition + 1].getTile() != null) {
			endPosition++;
		}

		Tile newTile = word.getTiles().get(wordIndex);

		// construct crossword and count tiles already on the board
		StringBuilder sb = new StringBuilder();
		int score = 0;
		for (int i = startPosition; i <= endPosition; i++) {
			if (i == line.getPosition()) {
				// skip the new tile
				sb.append(newTile.getLetter().getSymbol());
				continue;
			}
			Tile tile = crossingLine.getSquares()[i].getTile();
			sb.append(tile.getLetter().getSymbol());
			score += tile.getLetter().getValue();
		}

		// count the new tile, possibly with booster
		int letterValue = newTile.getLetter().getValue();
		Booster booster = line.getSquares()[position + wordIndex].getBooster();
		if (booster != null) {
			letterValue = letterValue * booster.letterFactor();
		}
		score += letterValue;

		if (booster != null) {
			score = score * booster.wordFactor();
		}
		String crossWord = sb.toString();
		boolean crossWordExists = language.getWords().contains(crossWord);
		if (crossWordExists) {
			word.setScore(word.getScore() + score);
		}
		return crossWordExists;
	}

	private boolean hasAdjacentTiles(Line line, Line crossingLine) {
		boolean tileBefore = line.getPosition() > 0
				&& crossingLine.getSquares()[line.getPosition() - 1].getTile() != null;
		boolean tileAfter = line.getPosition() < (crossingLine.getSquares().length - 1)
				&& crossingLine.getSquares()[line.getPosition() + 1].getTile() != null;
		return tileBefore || tileAfter;
	}

	public void play(Word word) {
		word.play();
		tray.removeTiles(word.getTiles());
		score += word.getScore();
	}

	public Tray getTray() {
		return tray;
	}

	public Board getBoard() {
		return board;
	}

	public int getScore() {
		return score;
	}

	public String getName() {
		return name;
	}

	public boolean isCpuControlled() {
		return cpuControlled;
	}

	@Override
	public String toString() {
		return String.format("Player %s score=%s", name, score);
	}
}
