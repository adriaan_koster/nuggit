package nl.nuggit.words.play;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import nl.nuggit.words.board.Line;
import nl.nuggit.words.board.Square;
import nl.nuggit.words.board.Tile;

public class Word {

	/**
	 * Sorts words by their basic value from high to low.
	 */
	public static final Comparator<Word> COMPARATOR = new Comparator<Word>() {
		public int compare(Word word1, Word word2) {
			if (word1 == null && word2 == null) {
				return 0;
			} else if (word1 == null) {
				return 1;
			} else if (word2 == null) {
				return -1;
			}
			return word2.getScore() - word1.getScore();
		}
	};

	private final List<Tile> tiles = new ArrayList<Tile>();
	private final Line line;
	private final int startPosition;
	private int score;

	public Word(Line line, int startPosition) {
		this.line = line;
		this.startPosition = startPosition;
	}

	public void play() {
		int tilePosition = startPosition;
		for (Tile tile : tiles) {
			Square square = line.getSquares()[tilePosition];
			Tile boardTile = square.getTile();
			if (boardTile == null) {
				square.setTile(tile);
			} else if (boardTile != tile) {
				throw new IllegalStateException(String.format(
						"Tile mismatch board:%s, word:%s", boardTile, tile));
			}
			tilePosition++;
		}
	}

	public List<Tile> getTiles() {
		return tiles;
	}

	public Line getLine() {
		return line;
	}

	public int getStartPosition() {
		return startPosition;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void addScore(int increase) {
		this.score += increase;
	}

	public int size() {
		return tiles.size();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Tile tile : tiles) {
			sb.append(tile.getLetter().getSymbol());
		}
		sb.append("(").append(line).append(" startPosition ")
				.append(startPosition).append(" score ").append(score)
				.append(")");
		return sb.toString();
	}
}
