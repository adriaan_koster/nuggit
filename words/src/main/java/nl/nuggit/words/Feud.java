package nl.nuggit.words;

import nl.nuggit.words.board.BagOfTiles;
import nl.nuggit.words.board.Board;
import nl.nuggit.words.board.BoardFactory;
import nl.nuggit.words.board.Line;
import nl.nuggit.words.board.Tray;
import nl.nuggit.words.language.Language;
import nl.nuggit.words.play.Player;

public class Feud {

	public static void main(String[] args) {

		Language language = Language.dutch();
		BagOfTiles bag = new BagOfTiles(language);

		Board board = BoardFactory.createStandardBoard();
		setupBoard(board, language);
		System.out.println(board);

		Tray tray = new Tray("morlend", 7, language);

		bag.remove(board.getTiles());
		bag.remove(tray.getTiles());
		System.out.println(bag);

		Player player = new Player("adriaan", board, tray, language);
		player.suggestWords();
	}

	private static void setupBoard(Board board, Language language) {
		board.addWord("bermen", Line.Direction.ROW, 7, 6, language);
		board.addWord("kwakenDe", Line.Direction.COLUMN, 10, 3, language);
		board.addWord("jaagt", Line.Direction.ROW, 5, 9, language);
		board.addWord("opvouwen", Line.Direction.ROW, 10, 4, language);
		board.addWord("ent", Line.Direction.COLUMN, 11, 9, language);
		board.addWord("cos", Line.Direction.COLUMN, 4, 9, language);
		board.addWord("dooft", Line.Direction.COLUMN, 13, 1, language);
		board.addWord("cox", Line.Direction.COLUMN, 7, 9, language);
		board.addWord("snak", Line.Direction.ROW, 3, 7, language);
		board.addWord("hom", Line.Direction.ROW, 2, 12, language);
		board.addWord("gems", Line.Direction.COLUMN, 14, 0, language);
		board.addWord("sir", Line.Direction.COLUMN, 7, 3, language);
		board.addWord("veie", Line.Direction.COLUMN, 6, 10, language);
		board.addWord("hen", Line.Direction.COLUMN, 8, 1, language);
		board.addWord("sneer", Line.Direction.ROW, 5, 3, language);
		board.addWord("ze", Line.Direction.COLUMN, 12, 8, language);
		board.addWord("inbeuken", Line.Direction.ROW, 13, 3, language);
		board.addWord("yuCa", Line.Direction.ROW, 14, 0, language);
		board.addWord("talent", Line.Direction.ROW, 14, 9, language);
		board.addWord("volgt", Line.Direction.COLUMN, 14, 10, language);
		board.addWord("Deed", Line.Direction.ROW, 9, 10, language);
		
	}
}
