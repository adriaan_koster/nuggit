package nl.nuggit.words;

import java.util.List;

import nl.nuggit.words.board.Board;
import nl.nuggit.words.board.Tray;
import nl.nuggit.words.language.Language;
import nl.nuggit.words.play.Player;
import nl.nuggit.words.play.Word;

public class DrawSome {

	public static void main(String[] args) {

		String letters = "ehoiopqvwwwy";
		int wordlength = 3;

		Language language = Language.drawsome();
		Board board = new Board(wordlength);
		Tray tray1 = new Tray(letters, 12, language);
		Player player1 = new Player("p1", board, tray1, language);
		List<Word> words = player1.suggestWords();
		for (Word word : words) {
			if (word.size() == wordlength) {
				System.out.println(word);
			}
		}
	}
}
