package nl.nuggit.words.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import nl.nuggit.words.play.Game;

public class MainWindow extends JFrame implements MouseListener,
		MouseMotionListener {

	private static final long serialVersionUID = 1L;

	private JLayeredPane layeredPane;
	private JPanel boardPanel;
	private JTextPane tilePane;
	private int xAdjustment;
	private int yAdjustment;
	private Game game;

	public MainWindow() {

		game = new Game();
		game.addPlayer("p1", true);
		game.addPlayer("p2", true);

		Dimension windowSize = new Dimension(600, 800);
		layeredPane = new JLayeredPane();
		getContentPane().add(layeredPane);
		layeredPane.setPreferredSize(windowSize);
		layeredPane.addMouseListener(this);
		layeredPane.addMouseMotionListener(this);

		boardPanel = new JPanel();
		boardPanel.setLayout(new GridLayout(game.getBoard().size(), game
				.getBoard().size()));
		Dimension boardSize = new Dimension(400, 400);
		boardPanel.setPreferredSize(boardSize);
		boardPanel.setBounds(100, 10, boardSize.width, boardSize.height);
		layeredPane.add(boardPanel, JLayeredPane.DEFAULT_LAYER);
		for (int row = 0; row < game.getBoard().size(); row++) {
			for (int column = 0; column < game.getBoard().size(); column++) {
				boardPanel.add(new SquarePane(game.getBoard().getSquare(column,
						row)));
			}
		}

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(true);
		setLocationRelativeTo(null);
		pack();
		setVisible(true);
		game.play();
	}

	public void mousePressed(MouseEvent e) {
		tilePane = null;
		Component component = boardPanel.findComponentAt(e.getX(), e.getY());
		if (!(component instanceof TilePane) || component.getParent() == null) {
			return;
		}
		Point parentLocation = component.getParent().getLocation();
		xAdjustment = parentLocation.x - e.getX();
		yAdjustment = parentLocation.y - e.getY();
		tilePane = (TilePane) component;
		tilePane.setLocation(e.getX() + xAdjustment, e.getY() + yAdjustment);
		tilePane.setSize(tilePane.getWidth(), tilePane.getHeight());
		layeredPane.add(tilePane, JLayeredPane.DRAG_LAYER);
	}

	public void mouseDragged(MouseEvent me) {
		if (tilePane == null) {
			return;
		}
		tilePane.setLocation(me.getX() + xAdjustment, me.getY() + yAdjustment);
	}

	public void mouseReleased(MouseEvent e) {
		if (tilePane == null) {
			return;
		}
		tilePane.setVisible(false);
		Component component = boardPanel.findComponentAt(e.getX(), e.getY());
		if (component instanceof SquarePane) {
			SquarePane squarePane = (SquarePane) component;
			if (squarePane.getSquare().getTile() == null) {

			}
			Container parent = (Container) component;
			parent.add(tilePane);
		}

		tilePane.setVisible(true);
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}
}
