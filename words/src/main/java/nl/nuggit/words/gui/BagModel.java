package nl.nuggit.words.gui;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import nl.nuggit.words.board.BagOfTiles;
import nl.nuggit.words.board.Tile;

public class BagModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private BagOfTiles bag;

	private static final int rowCount = 8;
	private static final int columnCount = 13;

	public int getColumnCount() {
		return columnCount;
	}

	public int getRowCount() {
		return rowCount;
	}

	public Object getValueAt(int row, int col) {
		if (bag != null) {
			List<Tile> tiles = bag.getTiles();
			int index = (row * columnCount) + col;
			if (index < tiles.size()) {
				return tiles.get(index);
			}
		}
		return null;
	}

	public BagOfTiles getBagOfTiles() {
		return bag;
	}

	public void setBagOfTiles(BagOfTiles bag) {
		this.bag = bag;
	}
}
