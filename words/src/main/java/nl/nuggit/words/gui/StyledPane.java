package nl.nuggit.words.gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

public class StyledPane extends JTextPane {

	static final long serialVersionUID = 1L;
	static final Color COLOR_TILE = new Color(255, 255, 255);
	static final Color COLOR_BOARD = new Color(0, 0, 0);
	static final Color COLOR_DL = new Color(121, 163, 113);
	static final Color COLOR_TL = new Color(26, 139, 155);
	static final Color COLOR_DW = new Color(185, 118, 39);
	static final Color COLOR_TW = new Color(151, 66, 25);
	static final Color COLOR_CENTER = new Color(97, 64, 93);

	public StyledPane() {
		setLayout(new BorderLayout());
		setStyles(getStyledDocument());
	}

	private StyledDocument setStyles(StyledDocument doc) {
		Style def = StyleContext.getDefaultStyleContext().getStyle(
				StyleContext.DEFAULT_STYLE);
		Style regular = doc.addStyle("regular", def);
		StyleConstants.setFontFamily(def, "SansSerif");
		Style s = doc.addStyle("small", regular);
		StyleConstants.setFontSize(s, 10);
		s = doc.addStyle("large", regular);
		StyleConstants.setFontSize(s, 16);
		return doc;
	}

	void append(StyledDocument doc, String text, String style)
			throws BadLocationException {
		doc.insertString(doc.getLength(), text, doc.getStyle(style));
	}
}
