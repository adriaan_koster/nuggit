package nl.nuggit.words.gui;

import java.awt.Color;

import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

import nl.nuggit.words.board.Tile;
import nl.nuggit.words.language.Letter;

public class TilePane extends StyledPane {

	private static final long serialVersionUID = 1L;

	private Tile tile;

	public TilePane(Tile tile) {
		this.tile = tile;
		updateContents();
	}

	private void updateContents() {
		try {
			StyledDocument tileDoc = getStyledDocument();
			setBackground(COLOR_TILE);
			setForeground(Color.BLACK);
			Letter letter = tile.getLetter();
			append(tileDoc, String.valueOf(letter.getSymbol()), "large");
			if (letter.getValue() > 0) {
				append(tileDoc, String.valueOf(letter.getValue()), "small");
			}
		} catch (BadLocationException e) {
			throw new IllegalStateException(e);
		}
	}
}
