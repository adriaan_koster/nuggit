package nl.nuggit.words.gui;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import nl.nuggit.words.board.Tile;
import nl.nuggit.words.board.Tray;

public class TrayModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private Tray tray;

	public int getColumnCount() {
		return 7;
	}

	public int getRowCount() {
		return 1;
	}

	public Object getValueAt(int row, int col) {
		if (tray != null) {
			List<Tile> tiles = tray.getTiles();
			if (col < tiles.size()) {
				return tiles.get(col);
			}
		}
		return null;
	}

	public Tray getTray() {
		return tray;
	}

	public void setTray(Tray tray) {
		this.tray = tray;
	}
}
