package nl.nuggit.words.gui;

import java.awt.Color;

import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

import nl.nuggit.words.board.Square;
import nl.nuggit.words.language.Letter;

public class SquarePane extends StyledPane {

	private static final long serialVersionUID = 1L;

	private Square square;

	public SquarePane(Square square) {
		this.square = square;
		updateContents();
	}

	public void updateContents() {
		try {
			StyledDocument doc = getStyledDocument();
			doc.remove(0, doc.getLength());
			if (square.getTile() == null) {
				setForeground(Color.WHITE);
				if (square.getBooster() != null) {
					switch (square.getBooster()) {
					case DOUBLE_LETTER:
						setBackground(COLOR_DL);
						append(doc, "DL", "regular");
						break;
					case TRIPLE_LETTER:
						setBackground(COLOR_TL);
						append(doc, "TL", "regular");
						break;
					case DOUBLE_WORD:
						setBackground(COLOR_DW);
						append(doc, "DW", "regular");
						break;
					case TRIPLE_WORD:
						setBackground(COLOR_TW);
						append(doc, "TW", "regular");
						break;
					default:
						throw new IllegalArgumentException(
								"Cannot render booster " + square.getBooster());
					}
				} else {
					if (square.isCenterSquare()) {
						setBackground(COLOR_CENTER);
					} else {
						setBackground(COLOR_BOARD);
					}
				}
			} else {
				setBackground(COLOR_TILE);
				setForeground(Color.BLACK);
				Letter letter = square.getTile().getLetter();
				append(doc, String.valueOf(letter.getSymbol()), "large");
				if (letter.getValue() > 0) {
					append(doc, String.valueOf(letter.getValue()), "small");
				}
			}
		} catch (BadLocationException e) {
			throw new IllegalStateException(e);
		}
	}

	public Square getSquare() {
		return square;
	}
}
