package nl.nuggit.words.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

public class BagPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public BagPanel(BagModel bagModel) {
		setLayout(new BorderLayout());
		JTable table = new JTable();
		table.setModel(bagModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setCellSelectionEnabled(true);
		add(table);
	}
}
